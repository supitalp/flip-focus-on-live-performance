package global.interfaces;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import global.model.Signal;
import global.model.Tag;
import global.model.VectorChroma;


public interface iLoader {

	/** 
	 * Loads a Signal object (i.e. approximately an array of SliceAT) 
	 * from the hard drive.
	 * 
	 * @param pcm_filename
	 * @return an instance of a Signal
	 */
	public Signal loadSignalFromPCM(String pcmFilename);

	/**
	 * Loads a tag file (i.e. a file containing Tag objects)
	 * from the hard drive.
	 * 
	 * @param tagfile_filename
	 * @return an array of Tag objects
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public ArrayList<Tag> loadTagsFromTagFile(String tagfileFilename)
		throws FileNotFoundException, IOException, ClassNotFoundException;

	/**
	 * Loads a Chroma file (i.e. a file containing VectorChroma objects)
	 * from the hard drive.
	 * 
	 * @param chromafile_filename
	 * @return an array of VectorChroma objects
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 */
	public VectorChroma[] loadChromasFromChromaFile(String chromafileFilename)
		throws IOException, ClassNotFoundException;
}
