package global.interfaces;

@Deprecated
public interface TransmitterInQueue<T> {
	
	public void push(T t);

}
