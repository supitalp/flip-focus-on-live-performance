package global.interfaces;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import global.model.Tag;
import global.model.VectorChroma;


public interface iWriter {

	/**
	 * Write a tag file (series of Tag objects) into the hard drive.
	 * 
	 * @param tagTab array of Tag objects
	 * @param tagfileFilename
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void writeTagFile(ArrayList<Tag> tagList, String tagfileFilename)
		throws FileNotFoundException, IOException;

	/**
	 * Write a chroma file (series of VectorChroma objects) into the hard drive,
	 * so that they don't need to be recomputed later.
	 * 
	 * @param tagChroma array of VectorChroma objects
	 * @param chromafileFilename
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void writeChromaFile(VectorChroma[] tagChroma, String chromaFilename)
		throws FileNotFoundException, IOException;
}