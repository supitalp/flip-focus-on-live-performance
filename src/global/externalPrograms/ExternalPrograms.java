package global.externalPrograms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;

import global.IOManagement.IOManagement;
import global.model.Signal;


/**To call external programs
 * 
 * @author ferdinand
 *
 */
public class ExternalPrograms 
{

	private boolean timidityInstalled;
	private boolean imagemagickInstalled;
	
	public ExternalPrograms() throws IOException
	{
		timidityInstalled 	 = checkInstalled("timidity");
		imagemagickInstalled = checkInstalled("convert");
	}
	
	/**To check whether the external programs we need are installed
	 * 
	 * @param program : The program to check
	 * @return : A boolean to see whether the program is correctly installed
	 * @throws IOException
	 */
	public boolean checkInstalled (String program) throws IOException
	{
		Runtime runtime = Runtime.getRuntime();
		Process proc 	= runtime.exec("which "+program);
		
		//To catch the output stream from the external program 
		StringWriter writer 		   = new StringWriter();
		InputStreamReader streamReader = new InputStreamReader(proc.getInputStream());
		BufferedReader buffer		   = new BufferedReader(streamReader);
		String line="";
		while ( null!=(line=buffer.readLine())) {
			writer.write(line);
		}
		if (writer.toString().length()==0)
			return false;
		else return true; /**TODO Check that the "which" command returns a 0-length String on every OS (it's ok on MAC OSX)*/
	}

	/**To execute an external command on the shell from the JVM
	 * 
	 * @param command : The command to execute
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static final void executeThis(String command) throws IOException, InterruptedException
	{
		Runtime runtime = Runtime.getRuntime();
		Process proc 	= runtime.exec(command);
		
		//To catch the output stream from the external program 
		StringWriter writer 		   = new StringWriter();
		InputStreamReader streamReader = new InputStreamReader(proc.getInputStream());
		BufferedReader buffer		   = new BufferedReader(streamReader);
		String line="";
		while ( null!=(line=buffer.readLine())) {
			writer.write(line);
		}
		// Final output on the Eclipse console
		System.out.println(writer.toString());
		proc.waitFor(); //To wait until the subprocess called is finished
		
	}
	
	/**To make a .wav file from a .mid file thanks to Timidity++
	 * 
	 * @param midiFilename : The name of the .mid file
	 * @return The Signal wav file which has just been generated
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public final Signal convertMIDIToWavByTimidity(String midiFilename) throws IOException, InterruptedException
	{
		String program 	   = "timidity ";
		String option	   = " -Ow --output-mono -o "; //Options to generate a .wav mono
		int lengthName	   = midiFilename.length();
		String outFilename = midiFilename.substring(0, lengthName-4)+".wav"; //To change the in.mid into a out.wav
		executeThis(program+midiFilename+option+outFilename);
		IOManagement IO	   = new IOManagement();
		Signal sig 		   = IO.loadSignalFromPCM(outFilename);
		return sig;
	}
	
	/**To change the .pdf into a multiple .png file, in which each png is recording thanks to the pdf page number
	 * 
	 * @param pdfFilename : The name of the .pdf file
	 * @return THe name of the png file which has been created
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public final String convertPDFToImages(String pdfFilename) throws IOException, InterruptedException
	{
		String program 	   = "convert ";
		String option	   = " "; //In case, we can add some options
		int lengthName	   = pdfFilename.length();
		String outFilename = pdfFilename.substring(0, lengthName-4)+".png"; //To change the in.pdf into a out.png
		executeThis(program+pdfFilename+option+outFilename);
		return outFilename;
	}
	
	/** 
	 * @return : A boolean to check whether the conversion from midi to wav is possible
	 */
	public boolean isTimidityInstalled() {
		return timidityInstalled;
	}

	/** 
	 * @return : A boolean to check whether the conversion from pdf to png is possible
	 */
	public boolean isImagemagickInstalled() {
		return imagemagickInstalled;
	}


	
}
