package global.audio;

import javax.sound.sampled.*;
import java.io.*;

public class PCMFilePlayer implements Runnable {
    File file;
    AudioInputStream in;
    SourceDataLine line;
    int frameSize;
    byte[] buffer = new byte [32*1024];
    Thread playThread;
    boolean playing;
    boolean notYetEOF;

    public PCMFilePlayer (File f)
        throws IOException,
               UnsupportedAudioFileException,
               LineUnavailableException {
        file = f;
        in = AudioSystem.getAudioInputStream (f);
        AudioFormat format = in.getFormat();
        AudioFormat.Encoding formatEncoding = format.getEncoding();
        if (!(formatEncoding.equals (AudioFormat.Encoding.PCM_SIGNED) ||
               formatEncoding.equals (AudioFormat.Encoding.PCM_UNSIGNED)))
            throw new UnsupportedAudioFileException (file.getName() + " is not PCM audio");
        System.out.println ("got PCM format");
        frameSize = format.getFrameSize();
        DataLine.Info info =
            new DataLine.Info (SourceDataLine.class, format);
        System.out.println ("got info");
        line = (SourceDataLine) AudioSystem.getLine (info);
        System.out.println ("got line");
        line.open();
        System.out.println ("opened line");
        playThread = new Thread (this);
        playing = false;
        notYetEOF = true;
        playThread.start();
    }

    public void run() {
        int readPoint = 0;
        int bytesRead = 0;

        try {
            while (notYetEOF) {
                if (playing) {
                    bytesRead = in.read (buffer,
                                         readPoint,
                                         buffer.length - readPoint);
                    if (bytesRead == -1) {
                        notYetEOF = false;
                        break;
                    }
                    // Number of frames & Left over
                    int frames = bytesRead / frameSize;
                    int leftover = bytesRead % frameSize;
                    // send to line
                    line.write (buffer, readPoint, bytesRead-leftover);
                    // save the leftover bytes
                    System.arraycopy (buffer, bytesRead,
                                      buffer, 0,
                                      leftover);
                    readPoint = leftover;
                } else {
                    try { Thread.sleep (10);}
                    catch (InterruptedException ie) {}
                }
            } // while notYetEOF
            System.out.println ("reached eof");
            line.drain();
            line.stop();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            line.close();
        }
    }

    public void start() {
        playing = true;
        if (! playThread.isAlive())
            playThread.start();
        line.start();
    }

    public void stop() {
        playing = false;
        line.stop();
    }

    public SourceDataLine getLine() {
        return line;
    }

    public File getFile() {
        return file;
    }

	public boolean isNotYetEOF() {
		return notYetEOF;
	}

	public void setNotYetEOF(boolean notYetEOF) {
		this.notYetEOF = notYetEOF;
	}

}