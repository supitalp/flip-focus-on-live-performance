package global.audio;

import global.IOManagement.IOManagement;
import global.IOManagement.Sound;
import global.model.SliceAT;
import global.queues.Queues;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

/** This class handles audio input by microphone.
 * Outputs SliceAT in queue liveMicroToDSPBySliceATQueue.
 *
 */
public class LiveMicro{
    protected boolean running;
    private ByteArrayOutputStream out;
    private AudioFormat format;

    public LiveMicro(){
         getFormat();
    }

    private AudioFormat getFormat() {
    	format= new AudioFormat(44100, 16, 1, true, false);
    	return format;
    }

    /**
	 * Starts receiving data from the microphone and outputs SliceAT's in public queue liveMicroToDSPBySliceATQueue
	 * 
	 * @param waitingTimeInMs : waits before starting to record data
	 */
	public final void startRecording(long WaitingTimeInMS){
    	try {
    		final AudioFormat format = getFormat();
    		DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
    		final TargetDataLine line = (TargetDataLine)AudioSystem.getLine(info);
    		line.open(format);
    		//line.start();
    		Thread.sleep(WaitingTimeInMS);
    		Runnable runner = new Runnable() {
    			//int bufferSize = (int) format.getSampleRate()* format.getFrameSize();
    			int bufferSize = (int)IOManagement.SAMPLES_PER_SLICE;
    			byte buffer[] = new byte[bufferSize*2]; /* bufferSize * 2 but i have no idea why ! otherwise sound is cut in half ! */
    			int position = 0;
    			
    			public void run(){
    				line.start();
    				out = new ByteArrayOutputStream();
    				running = true;
    				while (running) {
    					int count = line.read(buffer, 0, buffer.length);
    					if (count > 0) {
    						out.write(buffer, 0, count);
    						Queues.liveMicroToDSPBySliceATQueue.push(new SliceAT(Sound.toDouble(buffer), position));
    						position ++;
    					}
    				}
    				line.stop();
    			}
    		};
    		Thread captureThread = new Thread(runner);
    		captureThread.start();
    	} catch (LineUnavailableException e) {
    		System.err.println("Line unavailable: " + e);
    		System.exit(-2);
    	} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }

	/**
	 * 
	 * @param fileName : file in which the entire sound must be recorded (in WAV format)
	 * @return 
	 * @throws IOException 
	 */
	public final AudioInputStream StopRecording() throws IOException{
		running = false;
		byte[] audio = out.toByteArray();
        InputStream input = new ByteArrayInputStream(audio);
        final AudioFormat format = getFormat();
		final AudioInputStream ais = new AudioInputStream(input, format,audio.length / format.getFrameSize());
		input.close();
		return ais;
    }
	
	public static void main (String args[]) throws Exception{
		LiveMicro liveMicro= new LiveMicro();
		liveMicro.startRecording(0);
		System.out.println("Starting to record...");
		Thread.sleep(6000);
		System.out.println("Stopped recording.");
		AudioInputStream audio = liveMicro.StopRecording();
		AudioSystem.write(audio, AudioFileFormat.Type.WAVE, new File("testsound.WAV"));
	}

	public final boolean getRunning() {
		return running;
	}
	
}
