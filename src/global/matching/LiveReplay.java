package global.matching;

import java.util.*;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import global.*;
import global.IOManagement.IOManagement;
import global.model.AppModel;
import global.model.Clock;
import global.model.Tag;
import global.audio.PCMFilePlayer;

/**This LiveReplay Class creates a thread to play the Live Performance, and gives to the model the partition page when there is a tag
 * 
 * Version 1.0
 */
public class LiveReplay implements Runnable
{
	private ArrayList<Tag> referenceTags;
	private Path 		   matches;
	private String 		   liveFilename;
	private AppModel 	   appModel;
	private Clock		   liveClock;
	private int			   slicesTolerance = 2;
	


	/**Constructor
	 * 
	 * @param referenceTags The reference Tags ArrayList
	 * @param matches The path extracted from the two signal
	 * @param liveFilename The file name of the live performance
	 * @param appModel The application model
	 *
	 */
	public LiveReplay (ArrayList<Tag> referenceTags, Path matches, String liveFilename, AppModel appModel){
		this.referenceTags = referenceTags;
		this.matches 	   = matches;
		this.liveFilename  = liveFilename;
		this.appModel 	   = appModel;
	}
	

	
	
	@Override
	/**While this thread is running, it creates a new OutputManagementThread to play the live performance, and alerts the model when it needs to turn a page
	 *  
	 */
	public void run(){
		int   slicePosition		 = 0;
		int   nextTag			 = 0;
		float convertTimeInSlice = IOManagement.SAMPLE_RATE/IOManagement.SAMPLES_PER_SLICE;
		PCMFilePlayer audioOut;
		try {
			audioOut  = new PCMFilePlayer(new File(liveFilename));
			liveClock = new Clock();
			while(audioOut.isNotYetEOF()){
				slicePosition = (int)((liveClock.getElapsedTimeInFrames()));
				//System.out.println(slicePosition);
				ArrayList<Integer> referencePos  = matches.refPosition(slicePosition); // referencePos contains the slice numbers in "ref" that match the current slice in "play"
				if(!referencePos.isEmpty()) {
					for(int k=0;k<referencePos.size();k++){
						if(referencePos.get(k)-referenceTags.get(nextTag).getPos()<slicesTolerance){
							appModel.turnPage(referenceTags.get(nextTag).getPageNumber());
							nextTag++;
						}
					}
				}
				try {
					Thread.sleep((long)(1000./convertTimeInSlice)); //to wait a Slice time between two tag's position checks.
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		catch (IOException e1) {
			e1.printStackTrace();
		}
		catch (UnsupportedAudioFileException e1) {
			e1.printStackTrace();
		}
		catch (LineUnavailableException e1) {
			e1.printStackTrace();
		}
	}

	/**slicesTolerance defines how margin of error we tolerate (in slices). By default, it has been set to 2 slices
	 * 
	 */
	public int getSlicesTolerance() {
		return slicesTolerance;
	}

	public void setSlicesTolerance(int slicesTolerance) {
		this.slicesTolerance = slicesTolerance;
	}

}
