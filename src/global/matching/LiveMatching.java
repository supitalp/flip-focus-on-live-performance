package global.matching;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import net.sf.image4j.codec.bmp.BMPEncoder;

import global.model.*;
import global.queues.Queues;

public class LiveMatching implements Runnable {
	
	public static int SIMILARITY_MATRIX_SIZE 	= 10000;
	public static int SPEED_TOLERANCE 		 	= 2;
	public static int SLICE_TOLERANCE		 	= 2;
	public static int SLICES_MATCHING_TOLERANCE = 25; /* Number of dtw to be performed each NB_SLICES_BETWEEN_DTW */
	public static int SLICES_MATCHING_TOLERANCE_ON_FIRST_DTW = 500;
	public static int NB_SLICES_BETWEEN_DTW = 50;
	public static int MAX_LIVESND_SIZE = 10000; /* default : 10000 corresponds to approximately 17 minutes = max length of the live sound */
	public static double tresholdPercentage = 0.70; /* a sound (ref or live) is considered started whenever a new sound sample has a amplitude superior to the former one, regarding to tresholdPercentage */
	public static int earlyTurningTimer = 8; /* Permits to anticipate page turning. Pages are turned earlyTurningTimer slices before the tag is hit */
	private VectorChroma[] ref;
	private Tag[] 		   tags;
	private AppModel	   model;
	private double[][] 	   c;
	Cell   currentCell;
	Cell startingCell;
	Cell pastCell;
	private double tempLength;
	BufferedImage image;
	Boolean isPlotEnabled;
	Boolean hasDTWModeStarted;
	int stoppingPlot;
	double slope;
	
	/**Constructor of LiveMatching
	 * 
	 * @param ref The VectorChroma's tab of the reference signal
	 * @param tags The Tags' tab in the reference signal
	 * @param model The AppModel
	 */
	public LiveMatching(VectorChroma[] ref, Tag[] tags, AppModel model) {
		this.ref   = ref;
		this.tags  = tags;
		this.model = model;
		c = new double[ref.length][MAX_LIVESND_SIZE];
		hasDTWModeStarted = false;
		stoppingPlot = 500; // maximum height of the image which will be outputted
		slope = 1.0;
		image = new BufferedImage(ref.length, stoppingPlot, BufferedImage.TYPE_INT_RGB);
	}
	
	/**
	 * 
	 * Starts listening to the microphone and calculating the matching path between ref sound and live sound.
	 * Turns pages whenever a tag has been reached.
	 * 
	 */
	public void run() {
		
		int sliceCptr 		= 0;
		int globalSliceCptr = 0;
		double formerVecAmplitude = 0;
		double formerRefVecAmplitude = 0;
		Boolean hasLiveSoundStarted = false;
		tempLength = 0;
		isPlotEnabled = false;
		double step = 0;
		double timeAtLoopBeginning = 0;
		int latestTagIndex = 0;
		
		for(int i=0;i<ref.length;i++) {
	           for(int j=0;j<MAX_LIVESND_SIZE;j++) {
	               c[i][j] = -1;
	           }
		}
		
		/* DEBUG : display tags ref position */
		for(int k=0;k<tags.length;k++) {
			System.out.println(tags[k].getPos());
		}
		
		// startingCell starts at the point where both ref and live sound have started (which means they have both reached a treshold amplitude) */
		currentCell 	 = new Cell(0,0);
		startingCell     = new Cell(0,0);
		pastCell = new Cell(0,0);
		
		/* search at which moment the sound has started on the reference sound */
		for(int i=0;i<ref.length;i++) {
			if(Math.log10(ref[i].getNorm()) > formerRefVecAmplitude + tresholdPercentage && formerRefVecAmplitude != 0) {
				startingCell.i = i;
				break;
			}
			else
				formerRefVecAmplitude = Math.log10(ref[i].getNorm());
		}
		
		VectorChroma vec;
		
		/* start listening to the microphone */
		while((vec = Queues.DSPToMatchingByVectorChromaQueue.pop()) != null) {
		
			timeAtLoopBeginning = System.currentTimeMillis();
			
			if(globalSliceCptr == stoppingPlot) {
				try {
					System.out.println("outputting image...");
					BMPEncoder.write(image, new File("output.bmp"));
					System.out.println("image outputted !");
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			/* Check whether live sound has started or not */
			if(Math.log10(vec.getNorm()) > formerVecAmplitude + tresholdPercentage && formerVecAmplitude != 0 && !hasLiveSoundStarted) {
				hasLiveSoundStarted = true;
				System.out.println("Live Sound has started at frame : " + globalSliceCptr);
				startingCell.j = globalSliceCptr;
				
				/* now that startingCell has been found, initialize the other cells */
				currentCell.i = startingCell.i;
				currentCell.j = startingCell.j;
				step = (double)startingCell.i;
				
				pastCell.i = startingCell.i;
				pastCell.j = startingCell.j;
				
			}
			else
				formerVecAmplitude = Math.log10(vec.getNorm());
			
			/* Updates the matching matrix */
		     for(int i=0;i<ref.length;i++) {
		        c[i][globalSliceCptr] = vec.scalarTo(ref[i]);
				int intensity = (int)(c[i][globalSliceCptr]*255);
				if(globalSliceCptr < stoppingPlot)
					image.setRGB(i, globalSliceCptr, new Color(intensity, intensity, intensity).getRGB());
		     }
		     globalSliceCptr++;
		     
		    if(hasLiveSoundStarted) { /* don't attempt at matching signals if live sound hasn't started yet ! */
		    	
		    	int i = 0, j = 0, k = 0, l = 0;
		    	
				if(sliceCptr < NB_SLICES_BETWEEN_DTW) { /* between two dtw's try navigating naively or slope-predictively throughout the grid*/
					
					sliceCptr++;
					
					if((globalSliceCptr - startingCell.j) <= NB_SLICES_BETWEEN_DTW) {
							
						/* naive navigation : at each new live slice, go to the heaviest coefficient in the grid
						 * this mode is used at the first iteration when the algorithm doesn't know in which direction it must go
						 * 
						 * note that the path can only go down or right-down (so the path follows the grid in time)
						 * 
						 *  */
						i = currentCell.i;
						j = currentCell.j;
						
						if(i==ref.length - 1) {
							j = j+1;
						}
						else if(j==globalSliceCptr-1) {
							i = i+1;
						}
						else {
							if(c[i+1][j+1] == min(c[i+1][j], c[i][j+1], c[i+1][j+1])) {
								i = i+1;
								j = j+1;
							}
							else if(c[i][j+1] == min(c[i+1][j], c[i][j+1], c[i+1][j+1])) {
								j = j+1;
							}
							else { // privilege the direction right-down
								i = i+1;
								j = j+1;
							}
						}
						
						currentCell.i = i; /* update currentCell */
						currentCell.j = j;
						
						if(i < ref.length && j < stoppingPlot && globalSliceCptr < stoppingPlot)
							image.setRGB(i, j, new Color(255, 0, 0).getRGB());
						
					}
					else { /* now that we have enough time samples, switch to slope-predictive mode */
						
						/* slope-predictive navigation : uses the latest dtw computing to predict the overall slope of the path and predicts the next NB_SLICES_BETWEEN_DTW
						 * cells of the path.
						 * this mode is used as soon as the first dtw has been computed (typically approx. 5s)
						 * 
						 */
						k = currentCell.i;
						l = currentCell.j;
						
						step += slope;
						k = (int)Math.ceil(step); /* we move along ref according to the slope */
						if(l != globalSliceCptr - 1)
							l++; /* we are forced to move down as time goes by */
						
						currentCell.i = k; /* update currentCell */
						currentCell.j = l;
						
						if(k < ref.length && l < stoppingPlot && globalSliceCptr < stoppingPlot)
							image.setRGB(k, l, new Color(255, 0, 255).getRGB());
					}	
				}
				else { /* time for the dtw correction : we try different paths along the line containing currentCell and choose the longest */
					
					sliceCptr = 0;
					Path currentPath = null;
					
					// Multiple DTW
					if(hasDTWModeStarted)
						currentPath = multipleBackTracking(startingCell, currentCell, SLICES_MATCHING_TOLERANCE);
					else
						currentPath = multipleBackTracking(startingCell, currentCell, SLICES_MATCHING_TOLERANCE_ON_FIRST_DTW);
					
					for(Cell c : currentPath.getPath()) {
						if(isPlotEnabled && c.i < ref.length)
							image.setRGB(c.i, c.j, new Color(0,255,0).getRGB());
					}
					
					// Upgrade mostProbableCell, currentCell & currentPredictedCell
					currentCell = currentPath.getPath().get(0);
					
					if(isPlotEnabled && currentCell.i < ref.length && currentCell.j < stoppingPlot)
						image.setRGB(currentCell.i, currentCell.j, new Color(255, 222, 0).getRGB());
					
					// Upgrade slope
					slope = (double)(currentCell.i - pastCell.i) / (double)(currentCell.j - pastCell.j);
					step = currentCell.i;
					System.out.println("speed  : " + (int)(slope * 100) + "%");
					
					pastCell.i = currentCell.i;
					pastCell.j = currentCell.j;
					
					hasDTWModeStarted = true;
					
					}
				}
		    
//		    System.out.println((System.currentTimeMillis() - timeAtLoopBeginning) + " ms.");
//		    System.out.println(currentCell.i + " " + currentCell.j + " " + globalSliceCptr);
		    
			/* check if a tag has been reached during the navigation */
		    int cpt = tags.length - 1;
		    while(cpt >= latestTagIndex) {
		    	if(currentCell.i > tags[cpt].getPos() - earlyTurningTimer) {
		    		model.turnPage(tags[cpt].getPageNumber());
		    		latestTagIndex = cpt + 1;
		    	}
		    	cpt--;
		    }  
		}
	}
	
	/**
	 * 
	 * Outputs an image displaying the current state of the matching process.
	 * 
	 * @param outputImageFileName
	 */
	public final void stop(String outputImageFileName) {
		try {
			System.out.println("outputting image...");
			BMPEncoder.write(image, new File(outputImageFileName));
			System.out.println("image outputted !");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * @param currentCell
	 * @param toleranceInSlices
	 * @return a path containing the longest path among all the possible paths starting from (0,0) to currentCell,
	 * with tolerance "toleranceInSlices"
	 */
	public final Path multipleBackTracking(Cell startingCell, Cell thisCell, int toleranceInSlices) {
		
		double[][] dtw = buildAccumulatedCostMatrix(ref.length, thisCell.j + 1); // perform the dtw only until cell "currentCell" + one line
		
		// create a list of possible final cells
		ArrayList<Cell> possibleFinalCells = new ArrayList<Cell>();
		for(int i=0;i<2*toleranceInSlices;i++) {
			if(thisCell.i-toleranceInSlices+i > 0 && thisCell.i-toleranceInSlices+i < ref.length)
				possibleFinalCells.add(new Cell(thisCell.i-toleranceInSlices+i, thisCell.j + 1)); // currentCell.j + 1 because we must progress in time even when performing dtw
		}
		
		for(Cell c : possibleFinalCells) {
			if(isPlotEnabled && c.i < ref.length && c.j < stoppingPlot)
				image.setRGB(c.i, c.j, new Color(0,0,255).getRGB());
		}
		
		// compute all the paths and correct their length, and store it into a list
		ArrayList<Path> possiblePaths = new ArrayList<Path>();
		ArrayList<Double> possibleLengths = new ArrayList<Double>();
		
		// backtracks for all possible final cells and stores the results in possiblePaths and possibleLengths
		for(Cell c : possibleFinalCells) {
			Path p = simpleBacktracking(startingCell, dtw, c);
			possiblePaths.add(p);
			
			possibleLengths.add(correctionalDistance(c)*tempLength);
			
			/* if the first cell of the path is too far from the starting cell, affect a highly negative coefficient for this path
			 * (which means its length must be set to zero)
			 *  */
			if(Math.abs(p.getPath().get(p.getPath().size()-1).i - startingCell.i) > 15)
				tempLength = 0;
			
//			possibleLengths.add(tempLength / p.getPath().size()); /* alternative way of correcting the length of the dtw. tests show it's less efficient */
		}
		
		// search for the longest length and return the corresponding path
		double max = 0;
		int maxIndex = 0, index = 0;
		for(Double d : possibleLengths) {
			if(d > max) {
				max = d;
				maxIndex = index;
			}
			index++;
		}
		return possiblePaths.get(maxIndex);
	}
	   /**
		 * Computes a simple backtracking using the accumulated cost matrix, from cell (0,0) to "finalCell".
		 * Modifies tempLength with the length of the path.
		 * 
		 * @param accumulated cost matrix (dtw)
		 * @param finalCell
		 * @return Path
		 */
		public final Path simpleBacktracking(Cell startingCell, double[][] dtw, Cell finalCell) {
			
			ArrayList<Cell> path = new ArrayList<Cell>();
			tempLength = 0;
			
			int i = finalCell.i-1;
			int j = finalCell.j-1;
			
			path.add(new Cell(i,j));
			
			while(i > startingCell.i && j > startingCell.j) {
				if(i==startingCell.i) {
					j = j-1;
				}
				else if(j==startingCell.j) {
					i = i-1;
				}
				else {
					if(dtw[i-1][j-1] == min(dtw[i-1][j], dtw[i][j-1], dtw[i-1][j-1])) {
						i = i-1;
						j = j-1;
					}
					else if(dtw[i][j-1] == min(dtw[i-1][j], dtw[i][j-1], dtw[i-1][j-1])) {
						j = j-1;
					}
					else {
						i = i-1;
					}
					path.add(new Cell(i,j));
					tempLength += c[i][j];
				}
			}
			
			return new Path(path);
		}
		
	   /** Builds the accumulated cost matrix.
	   The time cost of building this matrix is O(n*m).

	    * @return The accumulated cost matrix.
	    */
	   private final double[][] buildAccumulatedCostMatrix(int n, int m) {
	       
		   n = ref.length;
	       double[][] dtw = new double[n][m];
	       
	       dtw[0][0] = c[0][0];
	       
	       for(int i=1;i<n;i++) {
	           dtw[i][0] = dtw[i-1][0] + c[i][0];
	       }
	       
	       for(int j=1;j<m;j++) {
	           dtw[0][j] = dtw[0][j-1] + c[0][j];
	       }
	       
	       for(int i=1;i<n;i++) {
	           for(int j=1;j<m;j++) {
	               dtw[i][j] = c[i][j] + min(dtw[i-1][j], dtw[i][j-1], dtw[i-1][j-1]);
	           }
	       }
	       
	       return dtw;
	   }
	   
	   /** Minimum between three values
	    *
	    * @param a
	    * @param b
	    * @param c
	    * @return The minimum value between a, b and c.
	    */
	   private final double min(double a, double b, double c) {
	       if(a >= b && a >=c ) return a;
	       else if(b >= a && b >= c) return b;
	       else return c;
	   }
	   
	   /**Calculates the correctional coefficient for the DTW's Path
	    * In fact we normalize the path's length thanks to the Euclidean distance
	    * @param finalCell The final Cell of the current DTW path on which we normalize the length
	    * @return The correctional coefficient to multiply with the DTW length
	    */
	   public final double correctionalDistance(Cell finalCell) {
		   
		   double x = finalCell.i - startingCell.i;
		   double y = finalCell.j - startingCell.j;
		   return Math.sqrt((y*y) / (x*x + y*y));
		   
	   }
	   
		/** Draws the matching matrix and stores it in a file.
		 * 
		 * @param picFilename : filename of the picture to be drew
		 * @param currentPath : the Path found in with our multipleBacktracking method
		 * @param currentPos : the current position in the live play (in slices)
		 */
		public void outputToImage(String picFilename, Path currentPath, int currentPos, Cell[] expectedCell) {
			
			BufferedImage image = new BufferedImage(ref.length, currentPos, BufferedImage.TYPE_INT_RGB);
			
			for (int i=0;i<ref.length;i++){
				for(int j=0;j<currentPos;j++){
					int intensity = (int)(c[i][j]*255);
					image.setRGB(i, j, new Color(intensity, intensity, intensity).getRGB());
				}
			}
			
			for(Cell c : currentPath.getPath()) {
				if(c.i < ref.length && c.j < currentPos)
					image.setRGB(c.i, c.j, new Color(255, 0, 0).getRGB());
			}
			
			for(int i=0;i<expectedCell.length;i++) {
				if(expectedCell[i].i < ref.length && expectedCell[i].j < currentPos)
					image.setRGB(expectedCell[i].i, expectedCell[i].j, new Color(0,255,0).getRGB());
			}
			image.setRGB(startingCell.i, startingCell.j, new Color(0,0,255).getRGB());
			
			try {
				BMPEncoder.write(image, new File(picFilename+currentPos+".bmp"));
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}

}
