package global.matching;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/** The class Path represents a path in a similarity matrix (i.e the correspondances between the two signals)
 * It contains an ArrayList of Cell objects.
 * 
 * @author henri
 *
 */
public class Path {
	
	private ArrayList<Cell> path;
	
	/** Constructor of Path.
	 * 
	 * @param path : An ArrayList of Cell which constitute the path.
	 */
	public Path(ArrayList<Cell> path) {
		this.path = path;
	}
	
	/** Constructor of Path. Builds a path from a ".path" file.
	 * Format of ".path" files : several lines "a b" where "a" and "b" are coordinates in a matrix.
	 * 
	 * @param filepath : The file path of the ".path" file.
	 */
	public Path(String filepath) {
		Scanner pathReader;
		try {
			pathReader = new Scanner(new File(filepath));
			path = new ArrayList<Cell>();
			while(pathReader.hasNextInt()) {
				path.add(new Cell(pathReader.nextInt(), pathReader.nextInt()));
			}
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return the ArrayList of Cell objects representing the path
	 */
	public ArrayList<Cell> getPath() {
		return path;
	}
	
	@Override
	public String toString() {
		String chain = "";
		for(Cell c : path)
			chain += c.i + " " + c.j + "\n";
		return chain;
	}

	/** Checks if this path equals otherPath
	 * 
	 * @param otherPath : to be compared to this
	 * @return true if paths are equal, false otherwise
	 */
	public boolean equals(Path otherPath) {
		ArrayList<Cell> other = otherPath.getPath();
		if(path.size() == other.size()) {
			for(int k=0;k<path.size();k++) {
				if((path.get(k).i != other.get(k).i) || (path.get(k).j != other.get(k).j))
					return false;
			}
		}
		else
			return false;
		
		return true;
	}
	
	/**To find the matching Slice positions in the path.
	 * Be careful, the path must be added in the ArrayList in reverse order (from high to low values),
	 * otherwise it won't work !
	 * 
	 * @param j The live signal position 
	 * @return An ArrayList of the matching Slice positions
	 */
	public ArrayList<Integer> refPosition(int j){
		int k = 0;
		ArrayList<Integer> refPosition = new ArrayList<Integer>();
		while(path.size() > k && path.get(k).j>j-1){
			if(path.get(k).j == j)
				refPosition.add(path.get(k).i);
			k++;
		}
		return refPosition;
	}
}
