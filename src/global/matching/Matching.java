package global.matching;

import global.*;
import global.model.VectorChroma;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import net.sf.image4j.codec.bmp.BMPEncoder;

/** The class Matching handles the DTW algorithm which matches signals "ref" and "play" in time.
 * It contains the similarity matrix.
 * 
 * @author henri
 *
 */
public class Matching {
	
	private double[][] c; // Similarity matrix (c stands for cost). At cell (i,j) is the distance between slice i (ref) and slice j (play).
	private int n; // length of ref, in frames
	private int m; // length of play, in frames
	
	/** Constructor of Matching. Builds the similarity matrix.
	 * 
	 * @param ref : array of VectorChroma representing the ref signal
	 * @param play : array of VectorChroma representing the play signal
	 */
	public Matching(VectorChroma[] ref, VectorChroma[] play) {
		
		this.n = ref.length;
		this.m = play.length;
		c      = new double[n][m];
		
		for(int i=0;i<n;i++) {
			for(int j=0;j<m;j++) {
				c[i][j] = ref[i].scalarTo(play[j]);
			}
		}
	}
	
	/** Constructor of Matching. Builds the similarity matrix.
	 * 
	 * @param c : Similarity matrix
	 * @param n : length of ref, in frames
	 * @param m : length of play, in frames
	 */
	public Matching(double[][] c, int n, int m) {
		this.n = n;
		this.m = m;
		this.c = c;
	}
	
	/** Constructor of Matching.
	 * Builds a Matching object from a file stored in the hard drive.
	 * Format of a ".mat" file :
	 * First line : n m
	 * Other lines : double values with a space between each value
	 * 
	 * @param filepath : path of the ".mat" file
	 * @return A matching object containing the matrix stored in the file.
	 */
	public Matching(String filepath) {
		
		try {
			Scanner reader = new Scanner(new File(filepath));
			int i=0, j=0;
			this.n = reader.nextInt();
			this.m = reader.nextInt();
			c = new double[n][m];
			while(reader.hasNextInt()) {
				c[i][j] = reader.nextInt();
				j++;
				if(j>=m) {
					j=0;
					i++;
				}
			}
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return the n
	 */
	public int getN() {
		return n;
	}

	/**
	 * @return the m
	 */
	public int getM() {
		return m;
	}

	
	/** Matches both signals, i.e. performs the DTW algorithm.
	 * 
	 * @return An instance of Path containing the matches between two signals.
	 */
	public final Path match() {
		
		double[][] dtw = buildAccumulatedCostMatrix();
		return getWarpingPath(dtw);
	}
	
	/** Builds the accumulated cost matrix.
	The time cost of building this matrix is O(n*m).

	 * @return The accumulated cost matrix.
	 */
	private final double[][] buildAccumulatedCostMatrix() {
		
		double[][] dtw = new double[n][m];
		
		dtw[0][0] = c[0][0];
		
		for(int i=1;i<n;i++) {
			dtw[i][0] = dtw[i-1][0] + c[i][0];
		}
		
		for(int j=1;j<m;j++) {
			dtw[0][j] = dtw[0][j-1] + c[0][j];
		}
		
		for(int i=1;i<n;i++) {
			for(int j=1;j<m;j++) {
				dtw[i][j] = c[i][j] + min(dtw[i-1][j], dtw[i][j-1], dtw[i-1][j-1]);
			}
		}
		
		return dtw;
	}
	
	/** Gets the warping path (optimum path between two signals) by backtracking from endpoint to startpoint
	 * in the accumulated cost matrix.
	 * 
	 * @param The accumulated cost matrix.
	 * @return An instance of Path containing the correspondances between the two signals.
	 */
	private final Path getWarpingPath(double[][] dtw) {
		
		ArrayList<Cell> path = new ArrayList<Cell>();
		
		int i = n-1;
		int j = m-1;
		
		path.add(new Cell(i,j));
		
		while(i > 0 && j > 0) {
			if(i==0) {
				j = j-1;
			}
			else if(j==0) {
				i = i-1;
			}
			else {
				if(dtw[i-1][j-1] == min(dtw[i-1][j], dtw[i][j-1], dtw[i-1][j-1])) {
					i = i-1;
					j = j-1;
				}
				else if(dtw[i][j-1] == min(dtw[i-1][j], dtw[i][j-1], dtw[i-1][j-1])) {
					j = j-1;
				}
				else {
					i = i-1;
				}
				path.add(new Cell(i,j));
			}
		}
		
		return new Path(path);
	}
	
	/** Minimum between three values
	 * 
	 * @param a
	 * @param b
	 * @param c
	 * @return The minimum value between a, b and c.
	 */
	private final double min(double a, double b, double c) {
		if(a >= b && a >=c ) return a;
		else if(b >= a && b >= c) return b;
		else return c;
	}
	
	/** Draws the matching matrix and stores it in a file.
	 * 
	 * @param picFilename : filename of the picture to be drew
	 * @param withPath : true if the dtw path should be also drew, false otherwise
	 */
	public void outputToImage(String picFilename, Boolean withPath) {
		
		BufferedImage image = new BufferedImage(n, m, BufferedImage.TYPE_INT_RGB);
		
		for (int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				int intensity = (int)(c[i][j]*255);
				image.setRGB(i, j, new Color(intensity, intensity, intensity).getRGB());
			}
		}
		
		if(withPath) {
			for(Cell c : match().getPath()) {
				image.setRGB(c.i, c.j, new Color(255, 0, 0).getRGB());
			}
		}

		try {
			BMPEncoder.write(image, new File(picFilename));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	

}
