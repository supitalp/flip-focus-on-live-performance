package global.matching;
import global.audio.*;
import global.model.VectorChroma;

import java.awt.Color;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import net.sf.image4j.codec.bmp.BMPEncoder;

/* Matrice de similarité. La case (i,j) contient la différence entre les slices i et j
 * respectivement du signal de référence et de celui joué en direct (ou simulé pour le prototype allégé)
 */
@Deprecated
public class MatchingMatrix {
	
	private double[][] matrix;
	private int n; // nbSlicesSignalReference
	private int m; // nbSlicesSignalToMatch
	
	/* Construction de la matrice de similarité.
	 * Prend en argument deux tableaux de VectorChroma (représentations compactes des slices).
	 * La distance entre deux slices correspond au produit scalaire entre deux vecteurs chromas
	 */
	public MatchingMatrix(VectorChroma[] refVector, VectorChroma[] playVector){
		int n  = refVector.length;
		int m  = playVector.length;
		matrix = new double[n][m];
		this.n = n;
		this.m = m;
		for (int i=0; i<n; i++){
			for(int j=0; j<m; j++){
				matrix[i][j]=refVector[i].scalarTo(playVector[j]);
			}
		}
	}
	
	public String toString(){
		String chaine="";
		for (int i=0; i<n; i++){
			chaine+="\n";
			for(int j=0; j<m; j++){
				chaine+=matrix[i][j] + " ";
			}
		}
		return chaine;
	}
	
	public int getN() {
		return n;
	}

	public int getM() {
		return m;
	}

	public void outputToImage(String filename) {
		
		BufferedImage image = new BufferedImage(n, m, BufferedImage.TYPE_INT_RGB);
		
		for (int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				int intensity = (int)(matrix[i][j]*255);
				if(matrix[i][j] >= 0)
					image.setRGB(i, j, new Color(intensity, intensity, intensity).getRGB());
				else
					image.setRGB(i, j, new Color(255, 0, 0).getRGB());
			}
		}

		try {
			BMPEncoder.write(image, new File(filename));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/* Algorithme naïf pour trouver un long chemin (pas optimal).
	 * Pour chaque case, on cherche sa voisine de coef max, et on itère.
	 * Remplit mat avec des -1 pour indiquer les portions de chemin
	 */
	public void naiveLongestPath() {
		int i = 0, j = 0;
		double droite = 0, bas = 0;
		matrix[0][0] = -1;
		while(i < n && j < m) {
			/* Test sur les deux directions possibles (bas, droite) */
			if(i+1 < n) droite = matrix[i+1][j];
			if(j+1 < m) bas = matrix[i][j+1];
			
			/* On prend la direction de coefficient max */
			if(bas >= droite) {
				if(j+1 < m) {
					matrix[i][j+1] = -1;
				}
				j = j+1;
			}
			else {
				if(i+1 < n) {
					matrix[i+1][j] = -1;
				}
				i = i+1;
			}
		}
	}
	
	/* Trouve le plus long chemin de départ jusqu'à arrivée (remplit la matrice mat avec des -1 pour indiquer les
	 * portions de chemin
	 */
	public void longestPath() {
		/* Principe : pour chaque case, on explore ses voisins et on les marque (si non déjà marquée : distance actuelle ;
		 * si déjà marquée : on marque de la distance actuelle, uniquement si elle est supérieure à celle marquée
		 * À la fin du marquage, on revient en arrière pour construire le plus grand chemin
		 */
		
		ArrayList<Case> listCasesToExplore = new ArrayList<Case>();
		double[][] matDistance = new double[n][m];
		for(int i=0;i<n;i++) { // Toutes les cases sont initialement non marquées
			for(int j=0;j<m;j++) {
				matDistance[i][j] = -1;
			}
		}

		Boolean continuer = true;
		
		Case depart = new Case(0,0), arrivee = new Case(n-1, m-1);
		matDistance[depart.getX()][depart.getY()] = 0;
		
		listCasesToExplore.add(depart);
		
		while(!listCasesToExplore.isEmpty() && continuer) { // Pour tous les successeurs et tant qu'on n'a pas atteint l'arrivée
			Case c = listCasesToExplore.get(0); // On le marque et on rajoute ses successeurs à la liste des cases à explorer
			if(!c.equals(arrivee))
				markSuccessors(c, listCasesToExplore, matDistance);
			else {
				continuer = false;
			}
		}
		
		/*
		for(int i=0;i<n;i++) {
			for(int j=0;j<m;j++) {
				System.out.print(matDistance[i][j] + " ");
			}
			System.out.println();
		}*/
		
		
		/* À ce stade, il suffit de remonter le plus long chemin à partir de l'arrivée et de marquer les points atteint */
		Case c = arrivee;
		int i = arrivee.getX(), j = arrivee.getY();
		matrix[i][j] = -1;
		while(i>0 && j>0) {
			/* on remonte , soit par en haut, soit à gauche */
			double gauche = 0, haut = 0;
			if(i-1 >= 0) gauche = matDistance[i-1][j];
			if(j-1 >= 0) haut = matDistance[i][j-1];
			
			if(haut >= gauche && j-1>=0) {
				matrix[i][j-1] = -1;
				System.out.println(i*4096.0/44100.0 + " " + (j-1)*4096.0/44100.0);
				c = new Case(i, j-1);
				j = j-1;
			}
			else {
				if(i-1 >=0) {
					matrix[i-1][j] = -1;
					System.out.println((i-1)*4096.0/44100.0 + " " + j*4096.0/44100.0);
				}
				c = new Case(i-1, j);
				i = i-1;
			}
		}
	}
	
	private void markSuccessors(Case c, ArrayList<Case> list, double[][] matDistance) {
		
		int i = c.getX(), j = c.getY();
		list.remove(c);
		
		/* Droite */
		if(i+1 < n) {
			if(matDistance[i+1][j] == -1) { // si case non marquée
				matDistance[i+1][j] = matDistance[i][j] + matrix[i][j]; // on marque et on rajoute à la liste des explorables
				list.add(new Case(i+1, j));
			}
			else { // si déjà marquée
				if(matDistance[i+1][j] <= matrix[i+1][j] + matDistance[i][j]) { // si la distance est supérieure à celle marquée
					matDistance[i+1][j] = matDistance[i][j] + matrix[i+1][j]; // on marque
				}
			}
		}
		
		/* Bas */
		if(j+1 < m) {
			if(matDistance[i][j+1] == -1) { // si case non marquée
				matDistance[i][j+1]  = matDistance[i][j] + matrix[i][j]; // on marque et on rajoute à la liste des explorables
				list.add(new Case(i, j+1));
			}
			else { // si déjà marquée
				if(matDistance[i][j+1] <= matrix[i][j+1] + matDistance[i][j]) { // si la distance est supérieure à celle marquée
					matDistance[i][j+1] = matDistance[i][j] + matrix[i][j+1]; // on marque
				}
			}
		}
	}
	
	public class Case {
		private int i, j;
		public Case(int i, int j) {
			this.i = i;
			this.j = j;
		}
		
		public int getX() {return i;}
		public int getY() {return j;}
	}

}
