package global.matching;

/** This class represents a cell in a matrix. Attributes are public because this class is
	 * expected to be used as a structure.
	 * 
	 * @author henri
	 *
	 */
public class Cell {
	/** Constructor of Cell.
 * 
 * @param i
 * @param j
 */
	public Cell(int i, int j) {
		this.i = i;
		this.j = j;
	}
	public int i;
	public int j;
}
