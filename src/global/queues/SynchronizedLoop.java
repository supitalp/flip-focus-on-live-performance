package global.queues;

public class SynchronizedLoop {
	
	private boolean continuer = true ;
	
	public synchronized void startLoop()
	{
		continuer = true ;
		notify() ;
	}
	
	
	public synchronized void stopLoop()
	{
		continuer = false ;
		notify() ;
	}
	
	
	public final void loop()
	{
		while (true) {
			// Test before entering the loop body
			synchronized(this) {
				while (!continuer)
					try { wait() ; } catch(InterruptedException e) {} ;
			}
			
			// Loop body
			
			
		}
	}
}
