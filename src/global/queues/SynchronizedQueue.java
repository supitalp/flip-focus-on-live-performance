package global.queues;

import java.util.Queue;

public final class SynchronizedQueue<T> {
	/** Internal array. */
    private final T content[] ;

    /** Where to take the next popped socket. */
    private int nextSocketIdx = 0;

    /** Where to put the next pushed socket. */
    private int lastSocketIdx = 0 ;

    /** The mast to be applied instead of modulo. */
    private final int mask ;

    /** Constructor,
     *  @param internal length length of the queue, must be a power of 2
     */

    @SuppressWarnings("unchecked")
	public SynchronizedQueue(int length)
    	throws Exception
    {
    	if ((length < 0) || ((length & (length - 1)) != 0))
    		throw new Exception("Invalid synchronized queue length: must be a power of 2.") ;

    	this.mask    = length - 1 ;
    	this.content = (T[]) new Object[length] ;
    }

    /** Pops a socket from the queue (blocking),
     *  returns next T in the queue
     */

    public final synchronized T pop()
    {
    	T result = null ;

    	try {

    		do {
    			if (nextSocketIdx == lastSocketIdx) {
    				wait() ; // The queue is empty
    			} else {
    				result = content[nextSocketIdx] ;
    				nextSocketIdx = ((nextSocketIdx + 1) & mask) ;
    				notify() ;
    				break ;
    			}
    		} while(true) ;

    	} catch (Exception e) {
    		e.printStackTrace() ;
    	}

    	return result ;
    }

    /** Pushes a socket in the queue (blocking). 
     *  @param t a T to be pushed in the queue
     */

    public final synchronized void push(T t)
    {
    	try {

    		do {
    			int futureIdx = ((lastSocketIdx + 1) & mask) ;
    			if (futureIdx == nextSocketIdx) {
    				wait() ; // The queue is full
    			} else {
    				content[lastSocketIdx] = t ;
    				lastSocketIdx = futureIdx ;
    				notify() ;
    				return ;
    			}
    		} while (true) ;

    	} catch (Exception e) {
    		e.printStackTrace() ;
    	}
    }
}
