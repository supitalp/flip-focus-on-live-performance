package global.queues;

import global.model.SliceAT;
import global.model.VectorChroma;
import global.queues.SynchronizedQueue;

/** This class contains the public queues that connect the different modules of the program.
 * 
 *
 */
public class Queues {
	
	public static SynchronizedQueue<SliceAT> liveMicroToDSPBySliceATQueue;
	public static SynchronizedQueue<VectorChroma> DSPToMatchingByVectorChromaQueue;
	public static SynchronizedQueue<VectorChroma> DSPToPlotByVectorChromaQueue;

}

