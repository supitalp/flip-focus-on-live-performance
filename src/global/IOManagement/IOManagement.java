/*
 * %W% %E% Ariel Otilibili Anieli
 */

package global.IOManagement;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import global.interfaces.iLoader;
import global.interfaces.iWriter;
import global.model.Signal;
import global.model.Tag;
import global.model.VectorChroma;

/**
 * Handles I/O.
 * Also contains global variables for the project.
 * 
 * @version 1.1
 * @author Ariel Otilibili Anieli, Bassirou Traoré, Henri Rebecq
 */
public class IOManagement implements iLoader, iWriter {

	/* Path of the folder where we should READ/WRITE */
	public String folderPath;


	/* Parameters of the audio line. */
	public final static float   SAMPLES_PER_SLICE   = 4096; // 4096 for live matching, 131072 is a good value for analysis mode
	public final static float   SAMPLE_RATE         = 44100; // Hertz

	public final static int     CHANNELS    = 1;
	public final static int     SAMPLE_SIZE = 16; // bits

	public final static boolean SIGNED     = false;
	public final static boolean BIG_ENDIAN = false;


	/** Default constructor */
	public IOManagement() {
	}

	/**
	 * Loads a signal from a PCM audio file.
	 * 
	 * @param pcmFilename PCM file name
	 */
	public Signal loadSignalFromPCM(String pcmFilename) {
		Sound pcmSound = new Sound(pcmFilename);
		return pcmSound.splitSignalToSlices();
	}

	/**
	 * Loads tags from a tag file on the hard disk.
	 * 
 	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @param tagFileName tag file name
	 * @return an array list of Tag
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Tag> loadTagsFromTagFile(String tagFileName)
		throws ClassNotFoundException, IOException {

		ArrayList<Tag>    list        = new ArrayList<Tag>();
		FileInputStream   fileStream  = new FileInputStream(tagFileName);
		ObjectInputStream inputStream = new ObjectInputStream(fileStream);

		list = (ArrayList<Tag>)inputStream.readObject();
		inputStream.close();

		return list;
	}

	/**
	 * Loads Chromas from a file on the hard disk.
	 * 
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @param chromaFileName tag file name
	 * @return an array list of Chromas 
	 */

	public VectorChroma[] loadChromasFromChromaFile(String chromaFileName)
		throws ClassNotFoundException, IOException {

		VectorChroma[]    list        = null;
		FileInputStream   fileStream  = new FileInputStream(chromaFileName);
		ObjectInputStream inputStream = new ObjectInputStream(fileStream);

		list = (VectorChroma[])inputStream.readObject();
		inputStream.close();

		return list;
	}

	/**
	 * Writes Tags on the hard disk.
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @param tagList list of Tags
	 * @param tagFileName file name on the hard disk
	 */
	public void writeTagFile(ArrayList<Tag> tagList, String tagFileName)
		throws FileNotFoundException, IOException {

		FileOutputStream   fileStream    = new FileOutputStream(tagFileName);
		ObjectOutputStream outputStream  = new ObjectOutputStream(fileStream);

		outputStream.writeObject(tagList);
		outputStream.close();
	}

	/**
	 * Writes Chromas on the hard disk.
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @param tagChroma list of Chromas
	 * @param chromaFileName file name on the hard disk
	 */

	public void writeChromaFile(VectorChroma[] tagChroma, String chromaFileName)
		throws FileNotFoundException, IOException {

		FileOutputStream   fileStream    = new FileOutputStream(chromaFileName);
		ObjectOutputStream outputStream  = new ObjectOutputStream(fileStream);

		outputStream.writeObject(tagChroma);
		outputStream.close();
	}
}