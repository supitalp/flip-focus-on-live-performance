package global.IOManagement;

import global.model.Signal;
import global.model.SliceAT;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.sound.sampled.*;

/** This class is designed to handle sound files.
 * It can load a PCM file to memory and create an instance of Signal based on it.
 * 
 * @author PACT 1.4
 *
 */
public class Sound {
   
    private AudioFormat format;
    private byte[] samples;
   
    /** Constructor of Sound. Loads a PCM file into the memory
     * 
     * @param filename : filename of the PCM file to be loaded
     */
    public Sound(String filename) {
        try {
            AudioInputStream stream = AudioSystem.getAudioInputStream(new File(filename));
            format = stream.getFormat();
            samples = getSamples(stream);
        }
        catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
    }
    catch (IOException e) {
            e.printStackTrace();
        }
    }
   
    public final byte[] getSamples() {
        return samples;
    }
    
    /**
     * 
     * @return the AudioFormat of the loaded sound
     */
    public final AudioFormat getFormat() {
    	return format;
    }
   
    /**
     * 
     * @param AudioInputStream stream
     * @return buffer of byte[]  
     */
    public final byte[] getSamples(AudioInputStream stream) {
        int length = (int)(stream.getFrameLength() * getFormat().getFrameSize());
        byte[] samples = new byte[length];
        DataInputStream in = new DataInputStream(stream);
        try{
            in.readFully(samples);
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return samples;
    }
   
    public static final double[] toDouble(byte[] b) {
        ByteBuffer bf = ByteBuffer.wrap(b).order(ByteOrder.LITTLE_ENDIAN);
        double[] table = new double[b.length/2];
        for(int i = 0 ; i < b.length/2 ; i ++){
            table[i] = (double)bf.getShort();
        }
        return table;
    }

       
    public final byte[] toByte(int[] signal) {
        ByteBuffer bb = ByteBuffer.allocate(signal.length*2);
        bb = bb.order(ByteOrder.LITTLE_ENDIAN);
        for(int i = 0 ; i < signal.length ; i ++)
                bb.putShort((short)signal[i]);
        return bb.array();
    }

    /** Encapsulates the sound (stored in the memory) into an instance of Signal.
     * 
     * @return an instance of Signal containing the sound, splitted into slices.
     */
    public final Signal splitSignalToSlices() {
    	
    	double[] doubleSamples = toDouble(samples);
    	int nbSlices = (int)Math.ceil(doubleSamples.length/IOManagement.SAMPLES_PER_SLICE) + 1; // Nombre de slices renvoyées
    	int i = 0; // Case du tableau dans laquelle sera ajouté la slice en cours
    	SliceAT[] tabSliceAT = new SliceAT[nbSlices];
    	
    	/* Pour un nombre nSlices de tranches de signal */
    	while(i<nbSlices) {
    		double[] currentSlice = new double[(int)IOManagement.SAMPLES_PER_SLICE];
    		for(int j=0;j<IOManagement.SAMPLES_PER_SLICE;j++) {
    			if(j+i*IOManagement.SAMPLES_PER_SLICE < doubleSamples.length)
    				currentSlice[j] = doubleSamples[j+i*(int)IOManagement.SAMPLES_PER_SLICE];
    			else
    				currentSlice[j] = 0;
    		}
    		tabSliceAT[i] = new SliceAT(currentSlice, i);
    		i++;
    	}
    	

    	return new Signal(tabSliceAT);
    }
  	
}
