package global.dsp;

import java.util.Observable;
import java.util.Observer;

import global.*;
import global.IOManagement.IOManagement;
import global.model.AppModel;
import global.model.Signal;
import global.model.SliceAF;
import global.model.SliceAT;
import global.model.VectorChroma;
import global.queues.Queues;
import global.queues.SynchronizedQueue;
import org.apache.commons.math.complex.Complex;
import org.apache.commons.math.transform.FastFourierTransformer;

/** The DSP class handles AudioProcessing (i.e. Fourier Transform (FFT algorithm) and chroma extraction).
 * Only one instance of this class should be created in the program.
 *
 * @author henri
 *
 */
public class LiveDSP {
	
	private FastFourierTransformer fft;
	
	/** Constructor of LiveDSP.
	 * 
	 * @param input : synchronized input queue, designed to receive SliceAT from the audio module
	 * @param output : synchronized output queue, designed to transmit VectorChroma to the UI model
	 * @param fft : an instance of a FastFourierTransformer which is used for FFT calculations
	 * @throws Exception 
	 */
	public LiveDSP(FastFourierTransformer fft) throws Exception {
		Queues.DSPToPlotByVectorChromaQueue = new SynchronizedQueue<VectorChroma>(256);
		Queues.liveMicroToDSPBySliceATQueue = new SynchronizedQueue<SliceAT>(256);
		Queues.DSPToMatchingByVectorChromaQueue = new SynchronizedQueue<VectorChroma>(256);
		this.fft    = fft;
	}
	
	/** Computes the FFT of the input sliceAT, i.e. the frequency spectrum.
	 * 
	 * @param sliceAT : amplitude-time frequency to be computed
	 * @param fft : an instance of a FastFourierTransformer, which should be initialized only once in the program
	 * @return A sliceAF containing the frequency information of the slice.
	 */
	public final SliceAF computeSliceSpectrum(SliceAT sliceAT) {
		
		double[] sliceAF = new double[(int)IOManagement.SAMPLES_PER_SLICE/2]; // Be careful, the SliceAF size is NB_SLICES/2, because of the FFT symmetry
		Complex[] out    = fft.transform(sliceAT.getSliceAT());
		
		for(int i=0;i<(int)IOManagement.SAMPLES_PER_SLICE/2;i++) {
			sliceAF[i] = out[i].abs();
		}
		
		return new SliceAF(sliceAF, sliceAT.getPos());
	}
	
	/** Transforms the input signal in VectorChromas (which are simplified representations of the signal
	 * 
	 * @param sig : instance of Signal (which is approximately an array of SliceAT)
	 * @return an array containing as NB_SLICE VectorChroma where NB_SLICE is the total number of slices in sig
	 */
	public final VectorChroma[] calculateChromaFrom(Signal sig) {
		
		int NB_SLICE = sig.getSig().length;
		SliceAT[] atTab = sig.getSig();
		VectorChroma[] chromaTab = new VectorChroma[NB_SLICE];
				
		for(int i=0; i < NB_SLICE; i++) {
		SliceAF temp = computeSliceSpectrum(atTab[i]);
		chromaTab[i] = temp.extractChromas();
		}
		
		return chromaTab;
	}
	
	public final void startComputing() {
		Runnable runner = new Runnable() {
			SliceAT currentSlice;
			/* each time a SliceAT is pushed in "input", a VectorChroma is computed and pushed in "output" */
			public void run(){
				while((currentSlice = Queues.liveMicroToDSPBySliceATQueue.pop()) != null) {
					Queues.DSPToPlotByVectorChromaQueue.push(computeSliceSpectrum(currentSlice).extractChromas());
				}
			}
		};
		Thread computeThread = new Thread(runner);
		computeThread.start();
	}
	
	public final void startComputingToMatch() {
		Runnable runner = new Runnable() {
			SliceAT currentSlice;
			/* each time a SliceAT is pushed in "input", a VectorChroma is computed and pushed in "output" */
			public void run(){
				while((currentSlice = Queues.liveMicroToDSPBySliceATQueue.pop()) != null) {
					Queues.DSPToMatchingByVectorChromaQueue.push(computeSliceSpectrum(currentSlice).extractChromas());
				}
			}
		};
		Thread computeThread = new Thread(runner);
		computeThread.start();
	}

}
