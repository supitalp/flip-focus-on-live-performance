package global.model;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import net.sf.image4j.codec.bmp.BMPEncoder;

import org.apache.commons.math.transform.FastFourierTransformer;

import global.IOManagement.IOManagement;
import global.audio.PCMFilePlayer;
import global.dsp.DSP;
import global.matching.Cell;
import global.matching.LiveReplay;
import global.matching.Matching;
import global.matching.Path;

public class Tests {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		FastFourierTransformer fft = new FastFourierTransformer();
		DSP dsp = new DSP(fft);
		IOManagement ioM = new IOManagement();
		
		Signal ref = ioM.loadSignalFromPCM("sounds/tests/minuet_bach/ref.wav");
		Signal play = ioM.loadSignalFromPCM("sounds/tests/minuet_bach/play.wav");
		
		Matching mat = new Matching(dsp.calculateChromaFrom(ref), dsp.calculateChromaFrom(play));
		mat.outputToImage("path.bmp", true);
		
		VectorChroma[] vecRef = dsp.calculateChromaFrom(ref);
		BufferedImage image = new BufferedImage(vecRef.length, 60, BufferedImage.TYPE_INT_RGB);
		
		/* Outputs the FAT image */
		for(int j=0;j<vecRef.length;j++) {
			for(int i=0;i<12;i++) {
				for(int k=0;k<5;k++) {
					int intensity = (int)(255*vecRef[j].get(i));
					image.setRGB(j, 59-(i*5+k), new Color(intensity, intensity, intensity).getRGB());
				}
			}
		}
		try {
			BMPEncoder.write(image, new File("fat.bmp"));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		ArrayList<Tag> refTags = new ArrayList<Tag>();
		refTags.add(new Tag(1200, "", 12));
		refTags.add(new Tag(2400, "", 17));
		refTags.add(new Tag(3600, "", 11));
		ArrayList<Cell> pathList =new ArrayList<Cell>();
		pathList.add(new Cell(90, 80));
		pathList.add(new Cell(60, 39));
		pathList.add(new Cell(30, 32));
		Path path = new Path(pathList);
		AppModel appmodel = new AppModel();
		
		LiveReplay live = new LiveReplay(refTags, path, "sounds/tests/minuet_bach/play.wav", appmodel);
		Thread liveThread = new Thread(live);
		liveThread.start();
		
		
		}
}
