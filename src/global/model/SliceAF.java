package global.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import global.*;
import global.IOManagement.IOManagement;

/** This class represents a small portion (called "slice" of a signal), in amplitude-frequency representation.
 * 
 * @author henri, ferdinand, johan
 *
 */
public class SliceAF {
	
	private double[] sliceAF; // represents amplitude/frequency (f = 44100*i / SAMPLES_PER_SLICE) of a slice
	private int pos; // Slice's position (in frames) in the reference signal
	
	/* Contains the reference frequencies for the piano notes */
	public static final double[] notesFrequencies = {
			(32.7032*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE), // C
			(34.6478*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE), // C#
			(36.7081*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE), // D
			(38.8909*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE), // D#
			(41.2034*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE), // E
			(43.6535*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE), // F
			(46.2493*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE), // F#
			(48.9994*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE), // G
			(51.9131*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE), // G#
			(27.5000*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE), // A
			(29.1352*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE), // A#
			(30.8677*IOManagement.SAMPLES_PER_SLICE/IOManagement.SAMPLE_RATE)}; // B
	
	private int nbHarmonics = 8; // Number of harmonics to be considered (default : 10)
	private int nbSamples = 2; //Number of samples to be taken for each frequency (default : 2)
	private int harmonicMin = 3; // First harmonic to be taken. lower this value to privilege low frequencies (default : 3)
	
	/** Constructor of SliceAF.
	 * 
	 * @param sliceAF : an array of doubles containing at cell i the amplitude of the frequency (f = 44100*i / SAMPLES_PER_SLICE)
	 * @param pos : the position of the slice in the reference signal, in number of frames
	 */
	public SliceAF(double[] sliceAF, int pos) {
		this.sliceAF = sliceAF;
		this.pos     = pos;
	}

	/** Extracts chromas from the slice (amplitude-frequency).
	 * A VectorChroma is a twelve-value vector containing the acoustic signature of a slice.
	 * 
	 * @return an instance of VectorChroma
	 */
	public final VectorChroma extractChromas() {
		
		double[] vecDouble = new double[notesFrequencies.length];
		for(int i=0;i<notesFrequencies.length;i++)
			vecDouble[i] = 0;
		
		for(int currentNote=0;currentNote<notesFrequencies.length;currentNote++) { // For each note in the scale
			
			double currentFreq = notesFrequencies[currentNote];
			
			for(int h=harmonicMin;h<nbHarmonics;h++) { // ...and nbHarmonics harmonics
				
				for(int e=0;e<nbSamples;e++) { // 2*nbSamples samples centered around currentFreq
					try {
						vecDouble[currentNote] += sliceAF[(int)(currentFreq*Math.pow(2,h) + e)]; // next harmonic is 2*currentFreq
						vecDouble[currentNote] += sliceAF[(int)(currentFreq*Math.pow(2,h) - e)];
					}
					catch(Exception exc) {
						System.out.println(exc);
					}
				}
			}
		}
		return new VectorChroma(vecDouble);
	}
	
	/** Saves this SliceAF in a GNUPlot displayable file.
	 * Amplitude = function(frequency (Hz))
	 * GNUPlot's usage example :
	 * > plot "A3.af" with lines
	 * > replot "A4.af" with lines
	 * 
	 * @param plotFilename : plot's filename
	 */
	public final void writeToPlotFile(String plotFilename) throws IOException {
        PrintWriter writer;
        writer =  new PrintWriter(new BufferedWriter
    	   (new FileWriter(plotFilename)));
       
        for(int i=0;i<sliceAF.length;i++) {
        	writer.println(IOManagement.SAMPLE_RATE*(double)i / IOManagement.SAMPLES_PER_SLICE + " " + sliceAF[i]);
        }
        writer.close();
	}
	
	/** 
	 * 
	 * @return the double sliceAF array.
	 */
	public final double[] getSliceAF() {
		return sliceAF;
	}
}