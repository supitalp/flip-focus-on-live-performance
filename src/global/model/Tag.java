package global.model;

import java.io.Serializable;

import global.IOManagement.IOManagement;

/** The Tag class represents a tag, which are associated with the reference Signal.
 * Each instance of Tag contains information about its position in the reference Signal, its associated page number, etc.
 * 
 * @author henri, ferdinand, johan
 *
 */
public class Tag implements Serializable {

	/* Serial number added. */
	private static final long serialVersionUID = 7451015299719719123L;

	private int pos; // Position of the tag in the reference signal, in frames
	private long posInMilliseconds; //Position of the tag in the reference signal, in milliseconds
	private String tagText; // For example the score's pdf filename
	private int pageNumber; // The page position in the partition
	// Any other relevant information about the tag
	
	public Tag(long posInMilliseconds, String tagText, int pageNumber) {
		this.posInMilliseconds = posInMilliseconds; 
		this.pos 			   = (int)((posInMilliseconds/1000.0)*(IOManagement.SAMPLE_RATE/IOManagement.SAMPLES_PER_SLICE)); // To convert the time position in frame position
		this.tagText 		   = tagText;
		this.pageNumber 	   = pageNumber;
	}

	public final long getPosInMilliseconds() {
		return posInMilliseconds;
	}

	public final int getPos() {
		return pos;
	}

	public final void setPos(int pos) {
		this.pos = pos;
	}

	public final String getTagText() {
		return tagText;
	}

	public final void setTagText(String tagText) {
		this.tagText = tagText;
	}

	public final int getPageNumber() {
		return pageNumber;
	}

	public final void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	public final static Tag getTag(String tagLine){
		String[] tab= tagLine.split(" ");
		return new Tag(Integer.parseInt(tab[0]), tab[1], Integer.parseInt(tab[2]));
	}
}

