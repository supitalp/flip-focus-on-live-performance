package global.model;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Observable;

import org.apache.commons.math.transform.FastFourierTransformer;

import global.queues.Queues;

import global.*;
import global.IOManagement.IOManagement;
import global.audio.LiveMicro;
import global.audio.PCMFilePlayer;
import global.dsp.DSP;
import global.dsp.LiveDSP;
import global.matching.LiveMatching;
import global.matching.Matching;
import global.matching.Path;
import global.queues.SynchronizedQueue;
import global.userInterface.view.PageTurnerFrame;
import global.userInterface.view.ViewModel;

/** Defines the model for the PageTurner application
 * 
 * @author PACT 1.4
 *
 */
public class AppModel extends Observable
{
	public VectorChroma vc;
	public Object LOCK = new Object();
	public static final long timeToWait = 3000;
	
	private String filenameRef = "";
	private String filenamePlay = "";
	private String filenameTagFile = "";
	private String filenameChromaFileRef = "";
	private String filenameChromaFilePlay = "";
	private ArrayList<Tag> tags = new ArrayList<Tag> ();
	private VectorChroma[] vecRef; //remplace null
	private VectorChroma[] vecPlay; //remplace null
	private Path matchingPath;
	private Signal ref;
	private Signal play;
	private Clock tagClock = new Clock();
	private LiveMatching liveMatching;
	private Matching matching;
	private DSP dsp;
	private LiveDSP liveDSP;
	private IOManagement ioManagement;
	private PCMFilePlayer audioOut;
	private LiveMicro liveMicro;
	private int currentPageNumber;
	private ViewModel viewModel;
	private boolean aboutToRecord = false;
	

	public AppModel() throws Exception {
		//Initialisation des modules utiles
		FastFourierTransformer fft = new FastFourierTransformer();
		dsp = new DSP(fft);
		ioManagement = new IOManagement ();
		currentPageNumber = 1;
		liveMicro = new LiveMicro();
		liveDSP = new LiveDSP(fft);
		viewModel = new ViewModel(this);
		//PageTurnerFrame pageTurnerFrame = new PageTurnerFrame(viewModel, this);
		
		play = new Signal();
		ref = new Signal();
	}
	
	/** This method is called by the matching module when it detects a tag has been reached.
	 * 
	 * @param pageNumber : next page's number
	 */
	public final void turnPage(int pageNumber) {
		System.out.println("turning page..." + pageNumber);
		viewModel.gotoPage(pageNumber);
	}

	public final String getFilenameRef() {
		return filenameRef;
	}

	public final void setFilenameRef(String filenameRef) {
		this.filenameRef = filenameRef;
	}

	public final String getFilenamePlay() {
		return filenamePlay;
	}

	public final void setFilenamePlay(String filenamePlay) {
		this.filenamePlay = filenamePlay;
	}

	public final String getFilenameTagFile() {
		return filenameTagFile;
	}

	public final void setFilenameTagFile(String filenameTagFile) {
		this.filenameTagFile = filenameTagFile;
	}

	public final String getFilenameChromaFileRef() {
		return filenameChromaFileRef;
	}

	public final void setFilenameChromaFileRef(String filenameChromaFileRef) {
		this.filenameChromaFileRef = filenameChromaFileRef;
	}

	public final String getFilenameChromaFilePlay() {
		return filenameChromaFilePlay;
	}

	public final void setFilenameChromaFilePlay(String filenameChromaFilePlay) {
		this.filenameChromaFilePlay = filenameChromaFilePlay;
	}

	public final ArrayList<Tag> getTags() {
		return tags;
	}

	public final void setTags(ArrayList<Tag> tags) {
		this.tags = tags;
	}

	public final VectorChroma[] getVecRef() {
		return vecRef;
	}

	public final void setVecRef(VectorChroma[] vecRef) {
		this.vecRef = vecRef;
	}

	public final VectorChroma[] getVecPlay() {
		return vecPlay;
	}

	public final void setVecPlay(VectorChroma[] vecPlay) {
		this.vecPlay = vecPlay;
	}

	public final Path getMatchingPath() {
		return matchingPath;
	}

	public final void setMatchingPath(Path matchingPath) {
		this.matchingPath = matchingPath;
	}

	public final Signal getRef() {
		return ref;
	}

	public final void setRef(Signal ref) {
		this.ref = ref;
	}

	public final Signal getPlay() {
		return play;
	}

	public final void setPlay(Signal play) {
		this.play = play;
	}

	public final DSP getDsp() {
		return dsp;
	}

	public final void setDsp(DSP dsp) {
		this.dsp = dsp;
	}

	public final IOManagement getIoManagement() {
		return ioManagement;
	}

	public final void setIoManagement(IOManagement ioManagement) {
		this.ioManagement = ioManagement;
	}

	public final PCMFilePlayer getAudioOut() {
		return audioOut;
	}

	public final void setAudioOut(PCMFilePlayer audioOut) {
		this.audioOut = audioOut;
	}

	public final int getCurrentPageNumber() {
		return currentPageNumber;
	}

	public final void setCurrentPageNumber(int currentPageNumber) {
		this.currentPageNumber = currentPageNumber;
	}

	public final Clock getTagClock() {
		return tagClock;
	}

	public final void setTagClock(Clock tagClock) {
		this.tagClock = tagClock;
	}

	public final ViewModel getViewModel() {
		return viewModel;
	}
	
	public Matching getMatching() {
		return matching;
	}

	public void setMatching(Matching matching) {
		this.matching = matching;
	}

	public final LiveMicro getLiveMicro() {
		// TODO Auto-generated method stub
		return this.liveMicro;
	}
	
	public final void setLiveMicro(LiveMicro liveMicro) {
		this.liveMicro = liveMicro;
	}
	
	public LiveMatching getLiveMatching() {
		return liveMatching;
	}

	public void setLiveMatching(LiveMatching liveMatching) {
		this.liveMatching = liveMatching;
	}
	
	public LiveDSP getLiveDSP() {
		return liveDSP;
	}
	
	public boolean isAboutToRecord() {
		return aboutToRecord;
	}

	public void setAboutToRecord(boolean aboutToRecord) {
		this.aboutToRecord = aboutToRecord;
	}


	public void repaint() {
		// TODO Auto-generated method stub
		viewModel.repaint();
	}
	 public void notifyForUpdate(){
		 viewModel.notifyForUpdate();
	 }

}