package global.model;

import global.IOManagement.IOManagement;

/** This class handles timing in the program. (especially for the tagging part).
 * 
 * @author PACT 1.4
 *
 */
public class Clock {
	
	double initialPos;
	double elapsedTime;
	Boolean isActive;
	
	/** Constructor.
	 * 
	 */
	public Clock() {
		reset();
	}
	
	/**
	 * 
	 * @return the elapsed time since last reset.
	 */
	public double getElapsedTime() {
		return System.currentTimeMillis() - initialPos;
	}
	
	/**
	 * 
	 * @return the elapsed time in frames since last reset.
	 */
	public double getElapsedTimeInFrames() {
		return (getElapsedTime() / 1000.0) * IOManagement.SAMPLE_RATE / IOManagement.SAMPLES_PER_SLICE;
	}
	
	/** Resets the clock.
	 * 
	 */
	public void reset() {
		initialPos = System.currentTimeMillis();
	}

}
