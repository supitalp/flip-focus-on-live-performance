package global.model;

import java.io.Serializable;

/** The VectorChroma class represents a simplified acoustic signature of a slice.
 * It encapsulates a twelve-value vector ; each value represents the intensity of one note, from A to G.
 * 
 * @author henri, ferdinand, johan
 * 
 *
 */
public class VectorChroma implements Serializable {
	
	private static final long serialVersionUID = 9087226174449380886L;
	private double[] tabChroma;
	private double norm; // either euclidian or maximum
	
	/** Constructor of VectorChroma.
	 * 
	 * @param tabChroma : array of doubles containing the 12 values of the vector
	 */
	public VectorChroma(double[] tabChroma) {
		this.tabChroma = tabChroma;
		this.norm      = this.normalizeEuclidian();
	}
	
	/**
	 * @return the tabChroma
	 */
	public final double[] getTabChroma() {
		return tabChroma;
	}
	
	/**
	 * @return the norm
	 */
	public double getNorm() {
		return norm;
	}

	/**

	/**
	 * @param tabChroma the tabChroma to set
	 */
	public final void setTabChroma(double[] tabChroma) {
		this.tabChroma = tabChroma;
	}

	/**
	 * 
	 * @param i : number of the value to be retrieved
	 * @return value number i of the vector
	 */
	public final double get(int i) {
		return tabChroma[i];
	}
	
	/** Outputs a string representation of the VectorChroma.
	 * 
	 * @return A string containing the twelve double values with a space between each value.
	 */
	public final String toString() {
		String chaine = "";
        for(int i=0;i<12;i++) {
        	chaine += tabChroma[i] + "\n";
        }
        return chaine;
	}
	
	/** Normalizes the vector (euclidian norm).
	 * 
	 * @return The euclidian norm of the vector.
	 */
	public final double normalizeEuclidian() {
		double norm = 0;
		for(int i=0;i<12;i++) {
			norm += tabChroma[i]*tabChroma[i];
		}
		norm = Math.sqrt(norm);
		for(int i=0;i<12;i++) {
			tabChroma[i] /= norm;
		}
		return norm;
	}
	
	/** Normalizes the vector (maximum norm)
	 * 
	 * @return the maximum value of the vector
	 */
	public final double normalizeMax() {
		double max = 0;
		for(int i=0;i<12;i++) {
			if(tabChroma[i] > max)
				max = tabChroma[i];
		}
		for(int i=0;i<12;i++) {
			tabChroma[i] /= max;
		}
		return max;
	}
	
	/** Compares a chroma to another one by computing the scalar product between two (i.e. the distance between two slices).
	 * A high value (maximum 1 for eulidian-normalized vectors) will mean both slices are highly similar.
	 * 
	 * @param vec : VectorChroma to be compared with
	 * @return The distance between the two VectorChroma (i.e. between the associated slices)
	 */
	public final double scalarTo(VectorChroma vec){
		double scalar=0;
		for(int i=0; i<12; i++){
			scalar += this.tabChroma[i]*vec.getTabChroma()[i];
		}
		return scalar;
	}
	
	public final static VectorChroma getVectorChroma(String chromaLine){
		String[] tab= chromaLine.split(" ");
		double[] vector= new double[12];
		for(int i=0; i < tab.length; i++){
			vector[i]= Double.parseDouble(tab[i]);
		}
		return new VectorChroma(vector);
	}
}
