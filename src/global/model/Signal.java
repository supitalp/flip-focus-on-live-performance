package global.model;


/**
 * The Signal Class represents all the signal (the reference one or the played), cut into N-size Slices.
 * It encapsulates a SliceAT-tab, and add a few methods.
 * 
 * @author henri, ferdinand, johan
 */
public class Signal {
	
	private SliceAT[] sig;

	/** Constructor of Signal
	 * 
	 * @param an array of sliceAT[] i.e. an array of slices (each slice of size SAMPLES_PER_SLICE)
	 */
	public Signal(SliceAT[] sig) {
		this.setSig(sig);
	}
	public Signal() {
	}
	
	public SliceAT[] getSig() {
		return sig;
	}

	public void setSig(SliceAT[] sig) {
		this.sig = sig;
	}
}
