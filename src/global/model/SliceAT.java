package global.model;

import global.IOManagement.IOManagement;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/** This class represents a small portion (called "slice" of a signal), in amplitude-time representation.
 * 
 * @author henri, ferdinand, johan
 *
 */
public class SliceAT {
	
	private double[] sliceAT; // represents amplitude/time of a slice (time is in frames)
	private int pos; // Slice's position (in frames) in the reference signal
	
	/** Constructor of SliceAT.
	 * 
	 * @param sliceAT : an array of doubles containing at cell i the amplitude of the sound at frame number i
	 * @param pos : the position of the slice in the reference signal, in number of frames
	 */
	public SliceAT(double[] sliceAT, int pos) {
		this.sliceAT = sliceAT;
		this.pos     = pos;
	}

	/**
	 * @return the array of doubles sliceAT
	 */
	public final double[] getSliceAT() {
		return sliceAT;
	}

	/**
	 * @return the position of the slice in the reference signal
	 */
	public final int getPos() {
		return pos;
	}
	
	/** Saves this SliceAT in a GNUPlot displayable file.
	 * Amplitude = function(time (ms))
	 * GNUPlot's usage example :
	 * > plot "A3.at" with lines
	 * > replot "A4.at" with lines
	 * 
	 * @param plotFilename : plot's filename
	 */
	public final void writeToPlotFile(String plotFilename) throws IOException {
        PrintWriter writer;
        writer =  new PrintWriter(new BufferedWriter
    	   (new FileWriter(plotFilename)));
        for(int i=0;i<sliceAT.length;i++) {
        	writer.println(1000*((double)i/IOManagement.SAMPLE_RATE) + " " + sliceAT[i]);
        }
        writer.close();
	}
}
