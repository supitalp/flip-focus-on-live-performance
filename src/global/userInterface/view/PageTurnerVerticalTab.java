package global.userInterface.view;

import global.model.AppModel;

import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

@Deprecated
/** This class will be used in further developments to support multiple files at the same time.
 * The VerticalTab handles all of the FilePanel used to support multiple files.
 * @author Adelin Travers
*/
public class PageTurnerVerticalTab extends JTabbedPane{
	//gère les onglets verticaux : font référence aux instances du programme
	
	public PageTurnerVerticalTab(String name, ViewModel viewModel, AppModel appModel) throws Exception{
		super();
		PageTurnerFilePanel filePanel = new PageTurnerFilePanel(viewModel, appModel);// � modifier lorsque l'on voudra consid�rer plusieurs fichiers
		this.addTab(name, filePanel);// à modifier pour afficher le texte en vertical sous forme d'icone(classe VTextIcon)
		this.setTabPlacement(SwingConstants.LEFT);
	}
}
