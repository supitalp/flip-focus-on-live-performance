package global.userInterface.view;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import global.model.AppModel;

public class JPGModel {
	
	private AppModel appModel;
	
	private JPGPanel jpgPanel1,jpgPanel2;
	
	private ArrayList<BufferedImage> jpgList;
	
	private ArrayList<String> docList;
	
	public JPGModel(AppModel appModel) {
		this.appModel = appModel;
		jpgPanel1 = new JPGPanel(appModel,this);
		jpgPanel2 = new JPGPanel(appModel,this);
		jpgList = new ArrayList<BufferedImage> ();
		docList = new ArrayList<String>();
	}
	
	
}
