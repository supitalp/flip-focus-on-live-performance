package global.userInterface.view;

import global.model.AppModel;
import global.userInterface.view.PDFUtilities.Flag;
import global.userInterface.view.PDFUtilities.FullScreenWindow;
import global.userInterface.view.PDFUtilities.PageChangeListener;
import global.userInterface.view.PDFUtilities.PagePanel;
import global.userInterface.view.PDFUtilities.PagePreparer;
import com.sun.pdfview.OutlineNode;
import com.sun.pdfview.PDFDestination;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFObject;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.action.GoToAction;
import com.sun.pdfview.action.PDFAction;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.AttributeSet;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
/**This class handles the opening and display of PDF files 
 * 
 * @author Adelin Travers : modified from an existing Java project: PDF-Renderer
 *
 */


public class ViewModel implements PageChangeListener{
	
	  public final static String TITLE = "Automatic Page-Turner";
	   
		/** The current PDFFile */
	    private PDFFile curFile;
	    /** the name of the current document */
	    private String docName;
	    private PagePanel PDFPanel, PDFPanel2;
	    /** The full screen page display, or null if not in full screen mode */
	    private PagePanel fspp;
	    /** The split between thumbs and page */
	    private JSplitPane split;
	    /** The PNG Model that takes care of the .png files associated to the score*/
	    private PNGModel pngModel;
	    MutableAttributeSet center = new SimpleAttributeSet();


	    //    Thread anim;
	    /** The current page number (starts at 0), or -1 if no page */
	    int curpage = -1;
	    /** flag to indicate when a newly added document has been announced */
	   private Flag docWaiter;
	    /** a thread that pre-loads the next page for faster response */
	   private PagePreparer pagePrep;
	    /** the window containing the pdf outline, or null if one doesn't exist */
	   private JDialog olf;
	    /** the current page number text field */
	  private JTextField pageField;
	    /** the root of the outline, or null if there is no outline */
	    OutlineNode outline = null;
	    
	    private final AppModel appModel;
	    private  final PageTurnerFrame frame;
	    
	    /**Constructor of the ViewModel
	     * 
	     * @param appModel
	     * @throws Exception 
	     */
	    
	    public ViewModel(AppModel appModel) throws Exception{
	    	this.appModel = appModel;
	    	PDFPanel = new PagePanel();
	    	PDFPanel2 = new PagePanel();
	    	pageField = new JTextField("-", 3);
	    	pngModel = new PNGModel(appModel);
	    	StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
	    	frame = new PageTurnerFrame(this,this.getAppModel());
	    }
	    
	  /**
	     * Changes the displayed page, desyncing if we're not on the
	     * same page as a presenter.
	     * @param pagenum the page to display
	     */
	    public void gotoPage(int pagenum) {
	        if (pagenum < 0) {
	            pagenum = 0;
	        } else if (pagenum >= curFile.getNumPages()) {
	            pagenum = curFile.getNumPages() - 1;
	        }
	        forceGotoPage(pagenum);
	    }
	    
	    public Icon getIcon(String name) {
	        Icon icon = null;
	        URL url = null;
	        try {
	            url = getClass().getResource(name);

	            icon = new ImageIcon(url);
	        } catch (Exception e) {
	            System.out.println("Couldn't find " + getClass().getName() + "/" + name);
	            e.printStackTrace();
	        }
	        return icon;
	    }
	    

	    /**
	     * Changes the displayed page.
	     * @param pagenum the page to display
	     */
	    public void forceGotoPage(int pagenum) {
	        if (pagenum <= 0) {
	            pagenum = 0;
	        } else if (pagenum >= curFile.getNumPages()) {
	            pagenum = curFile.getNumPages() - 1;
	        }
	        curpage = pagenum;

	        // update the page text field
	        pageField.setText(String.valueOf(curpage + 1));

	        // fetch the page and show it in the appropriate place
	        PDFPage pg = curFile.getPage(pagenum + 1);
	        if (fspp != null) {
	            fspp.showPage(pg);
	            fspp.requestFocus();
	        } else {
	            PDFPanel.showPage(pg);
	            PDFPanel.requestFocus();
	            PDFPanel2.showPage(pg);
	            PDFPanel2.requestFocus();
	        }


	        // stop any previous page prepper, and start a new one
	        if (pagePrep != null) {
	            pagePrep.quit();
	        }
	        pagePrep = new PagePreparer(pagenum, this);
	        pagePrep.start();

	        setEnabling();
	    }
	    /**
	     * Enable or disable all of the actions based on the current state.
	     */
	    public void setEnabling() {
	        boolean fileavailable = (curFile != null);
	        boolean pageshown = ((fspp != null) ? fspp.getPage() != null : PDFPanel.getPage() != null);

	        pageField.setEnabled(fileavailable);
	        closeAction.setEnabled(fileavailable);
	        prevAction.setEnabled(pageshown);
	        nextAction.setEnabled(pageshown);
	        firstAction.setEnabled(fileavailable);
	        lastAction.setEnabled(fileavailable);
	        fitInWindowAction.setEnabled(pageshown);
	    }
	    /**
	     * open a URL to a PDF file. The file is read in and processed
	     * with an in-memory buffer.
	     *
	     * @param url
	     * @throws java.io.IOException
	     */
	    public void openFile(URL url) throws IOException {
	        URLConnection urlConnection = url.openConnection();
	        int contentLength = urlConnection.getContentLength();
	        InputStream istr = urlConnection.getInputStream();
	        byte[] byteBuf = new byte[contentLength];
	        int offset = 0;
	        int read = 0;
	        while (read >= 0) {
	            read = istr.read(byteBuf, offset, contentLength - offset);
	            if (read > 0) {
	                offset += read;
	            }
	        }
	        if (offset != contentLength) {
	            throw new IOException("Could not read all of URL file.");
	        }
	        ByteBuffer buf = ByteBuffer.allocate(contentLength);
	        buf.put(byteBuf);
	        openPDFByteBuffer(buf, url.toString(), url.getFile());
	    }

	    /**
	     * <p>Open a specific pdf file.  Creates a DocumentInfo from the file,
	     * and opens that.</p>
	     *
	     * <p><b>Note:</b> Mapping the file locks the file until the PDFFile
	     * is closed.</p>
	     *
	     * @param file the file to open
	     * @throws IOException
	     */
	    public void openFile(File file) throws IOException {
	        // first open the file for random access
	        RandomAccessFile raf = new RandomAccessFile(file, "r");

	        // extract a file channel
	        FileChannel channel = raf.getChannel();

	        // now memory-map a byte-buffer
	        ByteBuffer buf =
	                channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
	        openPDFByteBuffer(buf, file.getPath(), file.getName());
	    }

	    /**
	     * <p>Open a specific pdf file.  Creates a DocumentInfo from the file,
	     * and opens that.</p>
	     *
	     * <p><b>Note:</b> By not memory mapping the file its contents are
	     * not locked down while PDFFile is open.</p>
	     *
	     * @param file the file to open
	     */
	    public void openFileUnMapped(File file) throws IOException {
	        DataInputStream istr = null;
	        try {
	            //load a pdf from a byte buffer
	            // avoid using a RandomAccessFile but fill a ByteBuffer directly
	            istr = new DataInputStream(new FileInputStream(file));
	            long len = file.length();
	            if (len > Integer.MAX_VALUE) {
	                throw new IOException("File too long to decode: " + file.getName());
	            }
	            int contentLength = (int) len;
	            byte[] byteBuf = new byte[contentLength];
	            int offset = 0;
	            int read = 0;
	            while (read >= 0) {
	                read = istr.read(byteBuf, offset, contentLength - offset);
	                if (read > 0) {
	                    offset += read;
	                }
	            }
	            ByteBuffer buf = ByteBuffer.allocate(contentLength);
	            buf.put(byteBuf);
	            openPDFByteBuffer(buf, file.getPath(), file.getName());
	        } catch (FileNotFoundException fnfe) {
	            fnfe.printStackTrace();
	        } catch (IOException ioe) {
	            ioe.printStackTrace();
	        } finally {
	            if (istr != null) {
	                try {
	                    istr.close();
	                } catch (Exception e) {
	                    // ignore error on close
	                }
	            }
	        }
	    }

	    /**
	     * open the ByteBuffer data as a PDFFile and start to process it.
	     *
	     * @param buf
	     * @param path
	     */
	    private void openPDFByteBuffer(ByteBuffer buf, String path, String name) {

	        // create a PDFFile from the data
	        PDFFile newfile = null;
	        try {
	            newfile = new PDFFile(buf);
	        } catch (IOException ioe) {
	            openError(path + " doesn't appear to be a PDF file." +
	                      "\n: " + ioe.getMessage ());
	            return;
	        }

	        // Now that we're reasonably sure this document is real, close the
	        // old one.
	        doClose();

	        // set up our document
	        this.curFile = newfile;
	        docName = name;
	        frame.setTitle(TITLE + ": " + docName);
	        
	        setEnabling();

	        // display page 1.
	        forceGotoPage(0);

	        // if the PDF has an outline, display it.
	        try {
	            outline = curFile.getOutline();
	        } catch (IOException ioe) {
	        }
	        if (outline != null) {
	            if (outline.getChildCount() > 0) {
	                olf = new JDialog(frame, "Outline");
	                olf.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	                olf.setLocation(frame.getLocation());
	                JTree jt = new JTree(outline);
	                jt.setRootVisible(false);
	                JScrollPane jsp = new JScrollPane(jt);
	                olf.getContentPane().add(jsp);
	                olf.pack();
	                olf.setVisible(true);
	            } else {
	                if (olf != null) {
	                    olf.setVisible(false);
	                    olf = null;
	                }
	            }
	        }
	    }

	    /**
	     * Display a dialog indicating an error.
	     */
	    public void openError(String message) {
	        JOptionPane.showMessageDialog(split, message, "Error opening file",
	                JOptionPane.ERROR_MESSAGE);
	    }
	    /**
	     * A file filter for PDF files.
	     */
	    FileFilter pdfFilter = new FileFilter() {

	        public boolean accept(File f) {
	            return f.isDirectory() || f.getName().endsWith(".pdf");
	        }

	        public String getDescription() {
	            return "Choose a PDF file";
	        }
	    };
	    private File prevDirChoice;

	    /**
	     * Ask the user for a PDF file to open from the local file system
	     */
	    public void doOpen() {
	        try {
	            JFileChooser fc = new JFileChooser();
	            fc.setCurrentDirectory(prevDirChoice);
	            fc.setFileFilter(pdfFilter);
	            fc.setMultiSelectionEnabled(false);
	            int returnVal = fc.showOpenDialog(frame);
	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	                try {
	                    prevDirChoice = fc.getSelectedFile();
	                    openFile(fc.getSelectedFile());
	                } catch (IOException ioe) {
	                    ioe.printStackTrace();
	                }
	            }
	        } catch (Exception e) {
	            JOptionPane.showMessageDialog(split,
	                    "Opening files from your local " +
	                    "disk is not available\nfrom the " +
	                    "Java Web Start version of this " +
	                    "program.\n",
	                    "Error opening directory",
	                    JOptionPane.ERROR_MESSAGE);
	            e.printStackTrace();
	        }
	    }

	    /**
	     * Open a local file, given a string filename
	     * @param name the name of the file to open
	     */
	    public void doOpen(String name) {
	        try {
	            URL url = new URL(name);
	            openFile(new URL(name));
	        } catch (IOException ioe) {
	            try {
	                openFile(new File(name));
	            } catch (IOException ex) {
	            }
	        }
	    }
	   
	    /**
	     * Close the current document.
	     */
	    public void doClose() {
	   
	        if (olf != null) {
	            olf.setVisible(false);
	            olf = null;
	        }
	        PDFPanel.showPage(null);
	        PDFPanel2.showPage(null);
	        curFile = null;
	        frame.setTitle(TITLE);
	        setEnabling();
	    }

	    /**
	     * Shuts down all known threads.  This ought to cause the JVM to quit
	     * if the PDFViewer is the only application running.
	     */
	    public void doQuit() {
	        doClose();
	        frame.dispose();
	        System.exit(0);
	    }

	    /**
	     * Turns on zooming
	     */
	    public void doZoomTool() {
	        if (fspp == null) {
	            PDFPanel.useZoomTool(true);
	        }
	    }

	    /**
	     * Turns off zooming; makes the page fit in the window
	     */
	    public void doFitInWindow() {
	        if (fspp == null) {
	            PDFPanel.useZoomTool(false);
	            PDFPanel.setClip(null);
	        }
	    }



	    /**
	     * Goes to the next page
	     */
	    public void doNext() {
	        gotoPage(curpage + 1);
	    }

	    /**
	     * Goes to the previous page
	     */
	    public void doPrev() {
	        gotoPage(curpage - 1);
	    }

	    /**
	     * Goes to the first page
	     */
	    public void doFirst() {
	        gotoPage(0);
	    }

	    /**
	     * Goes to the last page
	     */
	    public void doLast() {
	        gotoPage(curFile.getNumPages() - 1);
	    }

	    /**
	     * Goes to the page that was typed in the page number text field
	     */
	    public void doPageTyped() {
	        int pagenum = -1;
	        try {
	            pagenum = Integer.parseInt(pageField.getText()) - 1;
	        } catch (NumberFormatException nfe) {
	        }
	        if (pagenum >= curFile.getNumPages()) {
	            pagenum = curFile.getNumPages() - 1;
	        }
	        if (pagenum >= 0) {
	            if (pagenum != curpage) {
	                gotoPage(pagenum);
	            }
	        } else {
	            pageField.setText(String.valueOf(curpage));
	        }
	    }

	    
	    Action openAction = new AbstractAction("Open...") {

	        public void actionPerformed(ActionEvent evt) {
	            doOpen();
	        }
	    };
	    
	    Action closeAction = new AbstractAction("Close") {

	        public void actionPerformed(ActionEvent evt) {
	            doClose();
	        }
	    };
	    
	    Action quitAction = new AbstractAction("Quit") {

	        public void actionPerformed(ActionEvent evt) {
	            doQuit();
	        }
	    };

	    Action fitInWindowAction = new AbstractAction("Fit in window",getIcon("/com/sun/pdfview/gfx/fit.gif")
	            ) {

	        public void actionPerformed(ActionEvent evt) {
	            doFitInWindow();
	        }
	    };

	    
	  
	    // next and previous still need to be embedded with the setting of markers in the tag format
	    Action nextAction = new AbstractAction("Next", getIcon("/com/sun/pdfview/gfx/next.gif")
	    		) {

	        public void actionPerformed(ActionEvent evt) {
	            doNext();
	        }
	    };
	    Action firstAction = new AbstractAction("First", getIcon("/com/sun/pdfview/gfx/first.gif")
	    		) {

	        public void actionPerformed(ActionEvent evt) {
	            doFirst();
	        }
	    };
	    Action lastAction = new AbstractAction("Last", getIcon("/com/sun/pdfview/gfx/last.gif")
	    		) {

	        public void actionPerformed(ActionEvent evt) {
	            doLast();
	        }
	    };
	    Action prevAction = new AbstractAction("Prev", getIcon("/com/sun/pdfview/gfx/prev.gif")
	    		) {

	        public void actionPerformed(ActionEvent evt) {
	            doPrev();
	        }
	    };
	public PDFFile getCurFile() {
		return curFile;
	}
	public void setCurFile(PDFFile curFile) {
		this.curFile = curFile;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public PagePanel getFspp() {
		return fspp;
	}
	public void setFspp(PagePanel fspp) {
		this.fspp = fspp;
	}
	public int getCurpage() {
		return curpage;
	}
	public void setCurpage(int curpage) {
		this.curpage = curpage;
	}
	public Flag getDocWaiter() {
		return docWaiter;
	}
	public void setDocWaiter(Flag docWaiter) {
		this.docWaiter = docWaiter;
	}
	public PagePreparer getPagePrep() {
		return pagePrep;
	}
	public void setPagePrep(PagePreparer pagePrep) {
		this.pagePrep = pagePrep;
	}
	public JDialog getOlf() {
		return olf;
	}
	public void setOlf(JDialog olf) {
		this.olf = olf;
	}
	public PagePanel getPDFPanel() {
		return PDFPanel;
	}
	public void setPDFPanel(PagePanel pDFPanel) {
		PDFPanel = pDFPanel;
	}
	public PagePanel getPDFPanel2() {
		return PDFPanel2;
	}
	public void setPDFPanel2(PagePanel pDFPanel) {
		PDFPanel2 = pDFPanel;
	}
	public AppModel getAppModel(){
		return appModel;
	}

	public Action getOpenAction() {
		return openAction;
	}

	public Action getCloseAction() {
		return closeAction;
	}

	public Action getQuitAction() {
		return quitAction;
	}

	public Action getNextAction() {
		return nextAction;
	}

	public Action getPrevAction() {
		return prevAction;
	}

	public AttributeSet getCenter() {
		return center;
	}
	
	public PNGModel getPNGModel () {
		return this.pngModel;
	}

	public void repaint() {
		// TODO Auto-generated method stub
		frame.repaint();
	}

	public void notifyForUpdate() {
		frame.notifyForUpdate();
		
	}
}
