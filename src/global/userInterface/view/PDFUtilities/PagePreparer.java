package global.userInterface.view.PDFUtilities;

import global.userInterface.view.ViewModel;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

import java.awt.Dimension;
import java.awt.geom.Rectangle2D;


	 /**
     * A class to pre-cache the next page for better UI response
     */
public class PagePreparer extends Thread {
	
        int waitforPage;
        int curpage;
        int prepPage;
        ViewModel model;
        PagePanel page;
        PDFFile curFile;
        PagePanel fspp;

        /**
         * Creates a new PagePreparer to prepare the page after the current
         * one.
         * @param waitforPage the current page number, 0 based 
         */
        public PagePreparer(int waitforPage, ViewModel model) {
            setDaemon(true);
            setName(getClass().getName());
            
            this.model = model;
            page = model.getPDFPanel();
            curpage = model.getCurpage();
            curFile = model.getCurFile();
            fspp = model.getFspp();

            this.waitforPage = waitforPage;
            this.prepPage = waitforPage + 1;
        }
        public void quit() {
            waitforPage = -1;
        }

        public void run() {
            Dimension size = null;
            Rectangle2D clip = null;

            // wait for the current page
            //            System.out.println("Preparer waiting for page " + (waitforPage + 1));
            if (fspp != null) {
                fspp.waitForCurrentPage();
                size = fspp.getCurSize();
                clip = fspp.getCurClip();
            } else if (page != null) {
                page.waitForCurrentPage();
                size = page.getCurSize();
                clip = page.getCurClip();
            }

            if (waitforPage == curpage) {
                // don't go any further if the user changed pages.
                //                System.out.println("Preparer generating page " + (prepPage + 2));
                PDFPage pdfPage = curFile.getPage(prepPage + 1, true);
                if (pdfPage != null && waitforPage == curpage) {
                    // don't go any further if the user changed pages
                    //                    System.out.println("Generating image for page " + (prepPage + 2));

                    pdfPage.getImage(size.width, size.height, clip, null, true, true);
                //		    System.out.println("Generated image for page "+ (prepPage+2));
                }
            }
        }

}
