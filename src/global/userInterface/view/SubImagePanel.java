package global.userInterface.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.PaintContext;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import global.model.AppModel;
import global.userInterface.controllers.DrawPlot;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class SubImagePanel extends JPanel {
	
	BufferedImage image;
	/**Constructor of the SubImagePanel.
	 * Displays the image of the piano/ flip logo.
	 * 
	 * @param model
	 * @param appModel
	 * @throws IOException 
	 */
	public SubImagePanel(ViewModel model, AppModel appModel) throws IOException {
		super();
		image = ImageIO.read(new File("icons/keyboardPACT.jpg"));
		setPreferredSize(new Dimension(500,80));
		setBackground(Color.BLACK);
		this.repaint();
	}
	
	public void paintComponent(Graphics g) {
        g.drawImage(image, 0, 0, null);

    }

}
