package global.userInterface.view;


import global.model.AppModel;
import global.model.Clock;
import global.model.VectorChroma;
import global.userInterface.controllers.DrawPlot;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;


/**This class is used to display the plots of the chroma vectors.
 * 
 * @author Adelin Travers and Issam Outassourt
 *
 */

public class PlotPanel  extends JPanel {
	
	private final AppModel appModel;
	private JTextArea textArea;
	/**Constructor of the PlotPanel.
	 * For now the display is set to red to arrange the layout of the components on the LiveTab.
	 * 
	 * @param model
	 * @param appModel
	 * @throws Exception 
	 */
	public PlotPanel(ViewModel model, AppModel appModel) throws Exception {
		super();
		this.appModel = appModel;
		setPreferredSize(new Dimension(500,400));
		add(textArea = new JTextArea(""));
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		synchronized(appModel.LOCK) {
			DrawPlot.graphicAF(this, appModel.vc, g);
		}
	}
	
	public void paintTimeToGo(){
		Runnable runner = new Runnable() {
			public void run (){
				textArea.append("Time to record: ");
				textArea.setRows(1);
				int j = (int)appModel.timeToWait/1000;
				textArea.append(""+j);
				j--;
				while(j>0){
					try {
						Thread.sleep(1000);
						textArea.append(""+j);
						textArea.setSize(textArea.getSize().width+100, textArea.getSize().width+100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					j--;
				}
				textArea.removeAll();
			}
		};
		Thread chrono = new Thread(runner);
		chrono.start();
		appModel.setAboutToRecord(false);
		chrono.interrupt();
	}

	public void notifyForUpdate() {
		if(appModel.isAboutToRecord())
			paintTimeToGo();
	}
	
}
