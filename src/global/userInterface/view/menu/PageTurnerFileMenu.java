package global.userInterface.view.menu;

import global.model.AppModel;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class PageTurnerFileMenu extends JMenu {
	//génère le menu avec toutes ses entrées
	//penser à créer les classes pour les autres éléments du menu
	private final JMenuItem quit, newFile, openFile, save, saveAs, saveAll;
	public PageTurnerFileMenu(AppModel appModel) {
		super("File");
		
		this.add(newFile = new JMenuItem("New File..."));
		//newFile.addActionListener(listener(appModel));
		this.add(openFile = new JMenuItem("Open File..."));
		//idem pour le reste. On ne s'en charge pas pour l'instant
		this.add(save = new JMenuItem("Save"));
		this.add(saveAs = new JMenuItem("Save As..."));	
		this.add(saveAll = new JMenuItem("Save All"));
		
		this.add(quit = new JMenuItem("Quit"));
		
	}
}
