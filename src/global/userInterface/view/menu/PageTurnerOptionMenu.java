package global.userInterface.view.menu;

import global.model.AppModel;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class PageTurnerOptionMenu extends JMenu {
	
	private final JMenuItem setUp, extensionAcquisition, extensionManager, chooseDefaultSoundSystem;
	
	public PageTurnerOptionMenu(AppModel appModel){
		super("Option");
		this.add(chooseDefaultSoundSystem = new JMenuItem("Audio Peripherals..."));// permet de choisir les périphériques de son
		this.add(setUp = new JMenuItem("Set Up"));// checkbox permettant de désactiver l'affichage des graphes lors de l'enregistrement préliminaire
		this.add(extensionAcquisition = new JMenuItem("Add Extensions..."));// extension type métronome etc affichées dans des nouvelles fenetres
		this.add(extensionManager = new JMenuItem("Manage Extensions..."));// pop-up de type checkbox
		//ajouter les listener correspondants
	}

}
