package global.userInterface.view.menu;

import global.model.AppModel;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class PageTurnerHelpMenu extends JMenu {
	
	private final JMenuItem about, tutorial,doc;
	
	public PageTurnerHelpMenu(AppModel appModel) {
		super("Help");
		
		this.add(tutorial = new JMenuItem("Tutorial..."));//ouvre une fenetre donnant acces soit au tutoriel video soit au tutoriel ecrit
		this.add(doc = new JMenuItem("JavaDoc"));
		this.add(about = new JMenuItem("About..."));
		//ajouter les listener correspondants
	}

}
