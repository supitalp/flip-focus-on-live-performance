package global.userInterface.view.menu;

import global.model.AppModel;

import javax.swing.JMenuBar;

public class PageTurnerMenuBar extends JMenuBar{
	//gère la barre de menu
	private final PageTurnerFileMenu fileMenu;
	private final PageTurnerEditMenu editMenu;
	private final PageTurnerHelpMenu helpMenu;
	private final PageTurnerOptionMenu optionMenu;
	
	public PageTurnerMenuBar(AppModel appModel) {
		super();
		this.add(fileMenu = new PageTurnerFileMenu(appModel));
		this.add(editMenu = new PageTurnerEditMenu(appModel));
		this.add(optionMenu = new PageTurnerOptionMenu(appModel));
		this.add(helpMenu = new PageTurnerHelpMenu(appModel));

	}

}
