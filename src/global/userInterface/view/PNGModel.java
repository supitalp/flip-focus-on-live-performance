package global.userInterface.view;

import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;

import global.model.AppModel;

public class PNGModel {
	
	private AppModel appModel;
	
	private PNGPanel pngPanel1,pngPanel2;
	
	private ArrayList<BufferedImage> pngList;
	
	private int maxPageNumber = 0;
	
	private ArrayList<String> docList;
	
	public PNGModel(AppModel appModel) {
		this.appModel = appModel;
		pngPanel1 = new PNGPanel(appModel,this);
		pngPanel2 = new PNGPanel(appModel,this);
		pngList = new ArrayList<BufferedImage> ();
		docList = new ArrayList<String>();
	}
	
	public void setPNGFileList(ArrayList<File> fileList) throws IOException {
		pngList = new ArrayList<BufferedImage> ();
		for (File f : fileList) {
			pngList.add(ImageIO.read(f));
		}
		maxPageNumber = pngList.size();
		pngPanel1.setPageNumber(0);
		pngPanel2.setPageNumber(0);
	}
	
	public ArrayList<BufferedImage> getPNGList() {
		return this.pngList;
	}
	
	// Actions related to page changing
	
	Action nextPage1 = new AbstractAction("Next") {

        public void actionPerformed(ActionEvent evt) {
            pngPanel1.setPageNumber(Math.min(pngPanel1.getPageNumber()+1, maxPageNumber));
        }
    };
	Action nextPage2 = new AbstractAction("Next") {

        public void actionPerformed(ActionEvent evt) {
            pngPanel2.setPageNumber(Math.min(pngPanel1.getPageNumber()+1, maxPageNumber));
        }
    };	
	Action previousPage1 = new AbstractAction("Previous") {

        public void actionPerformed(ActionEvent evt) {
            pngPanel1.setPageNumber(Math.max(pngPanel1.getPageNumber()-1, 0));
        }
    };
	Action previousPage2 = new AbstractAction("Previous") {

        public void actionPerformed(ActionEvent evt) {
            pngPanel2.setPageNumber(Math.max(pngPanel2.getPageNumber()-1, 0));
        }
    };    
}
