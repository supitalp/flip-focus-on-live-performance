package global.userInterface.view;

import java.awt.Dimension;
import java.io.IOException;

import global.model.AppModel;
import global.userInterface.view.menu.PageTurnerMenuBar;

import javax.swing.JFrame;

/**The Frame used for the FLIP project.
 * 
 * @author Adelin Travers
 *
 */

public class PageTurnerFrame extends JFrame {
	
	private final AppModel appModel;
	private final PageTurnerWindowPanel pageTurnerWindowPanel; 
	/**Frame constructor
	 * 
	 * @param viewModel
	 * @param appModel
	 * @throws Exception 
	 */
	public PageTurnerFrame (ViewModel viewModel, AppModel appModel) throws Exception {
		super("Automatic-Pageturner");
		this.appModel = appModel;
		this.setJMenuBar(new PageTurnerMenuBar(appModel));
		pageTurnerWindowPanel = new PageTurnerWindowPanel(viewModel, appModel);
		this.add(pageTurnerWindowPanel);
		setPreferredSize(new Dimension(1200,1000));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		setExtendedState(this.MAXIMIZED_BOTH);
		pack();
		setVisible(true);
	}

	public void notifyForUpdate() {
		pageTurnerWindowPanel.notifyForUpdate();
		
	}
}
