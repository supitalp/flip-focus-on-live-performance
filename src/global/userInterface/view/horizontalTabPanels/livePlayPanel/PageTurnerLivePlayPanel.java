package global.userInterface.view.horizontalTabPanels.livePlayPanel;

import global.model.AppModel;
import global.userInterface.view.PageTurnerImagePanel;
import global.userInterface.view.ViewModel;
import global.userInterface.view.PDFUtilities.PagePanel;
import global.userInterface.view.horizontalTabPanels.EmptyPanel;
import global.userInterface.view.horizontalTabPanels.livePlayPanel.ExtensionPanel;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class PageTurnerLivePlayPanel extends JPanel {
	
	private final LivePlayPanelButtonPane buttonPane;
	private  PagePanel PDFPanel2;
	private final ExtensionPanel extensionPanel;
	private final ViewModel model;
	private final SouthPanel southPanel;
	public PageTurnerLivePlayPanel(ViewModel model, AppModel appModel){
			super();
			this.model = model;
			this.setLayout(new BorderLayout());
			add(buttonPane = new LivePlayPanelButtonPane(appModel), BorderLayout.NORTH);
			add(PDFPanel2 = model.getPDFPanel2(), BorderLayout.CENTER);
			add(extensionPanel = new ExtensionPanel(), BorderLayout.EAST);
			add(new EmptyPanel(0,10,10,10), BorderLayout.WEST);
			add(southPanel = new SouthPanel(model,appModel), BorderLayout.SOUTH);
			setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		}

}
