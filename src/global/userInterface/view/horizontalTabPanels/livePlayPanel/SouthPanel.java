package global.userInterface.view.horizontalTabPanels.livePlayPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;

import global.model.AppModel;
import global.userInterface.controllers.LoadLiveSound;
import global.userInterface.controllers.StartLivePlay;
import global.userInterface.view.ViewModel;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class SouthPanel extends JPanel {
		
		private final JTextPane annotationsField;


		public SouthPanel(ViewModel model,AppModel appModel) {
				super();
				setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
				setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
				add(Box.createRigidArea(new Dimension(50,20)));
				add(Box.createHorizontalGlue());
				add(Box.createRigidArea(new Dimension(50,20)));
				add(annotationsField = new JTextPane(), BorderLayout.SOUTH);
				//following lines should go in the controller 
				StyledDocument doc = annotationsField.getStyledDocument();
				annotationsField.setText("Hello world");
				doc.setParagraphAttributes(0, 0,model.getCenter(), true);
				annotationsField.setEditable(false);
				add(Box.createRigidArea(new Dimension(50,20)));
				add(Box.createHorizontalGlue());
				add(Box.createRigidArea(new Dimension(50,20)));


		}
}
