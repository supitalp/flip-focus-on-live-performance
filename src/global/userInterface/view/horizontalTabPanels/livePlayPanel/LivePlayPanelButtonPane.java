package global.userInterface.view.horizontalTabPanels.livePlayPanel;

import global.model.AppModel;
import global.userInterface.controllers.LoadLiveSound;
import global.userInterface.controllers.StartLivePlay;
import global.userInterface.controllers.StopLiveSound;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

public class LivePlayPanelButtonPane extends JPanel {
			
	private final JButton performButton/*, loadLiveSoundButton*/, stopLiveButton;

	public LivePlayPanelButtonPane(AppModel appModel) {
			super();
			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 2;
			c.gridy = 0;
			add(Box.createHorizontalGlue(),c);
			c.ipady = 60;      
			c.weightx = 1;
			c.gridwidth = 6;
			add(performButton = new JButton("Start playing!"),c);
			performButton.addActionListener(new StartLivePlay(appModel));
			c.weightx = 0.25;
			c.gridwidth = 1;
			c.ipady = 0;
			add(Box.createRigidArea(new Dimension(10,0)),c);
			c.weightx = 0;      
			c.gridwidth = 3;       
//			add(loadLiveSoundButton = new JButton("Load Fake Live Sound !"),c);
//			loadLiveSoundButton.addActionListener(new LoadLiveSound(appModel));
			add(stopLiveButton = new JButton("Output image"),c);
			stopLiveButton.addActionListener(new StopLiveSound(appModel));
			c.weightx = 2;
			c.gridwidth = 1;
			add(Box.createHorizontalGlue(),c);
			setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		}


}
