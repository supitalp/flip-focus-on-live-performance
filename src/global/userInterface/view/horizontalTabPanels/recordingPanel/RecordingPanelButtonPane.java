package global.userInterface.view.horizontalTabPanels.recordingPanel;

import global.userInterface.controllers.LoadReferenceSound;
import global.userInterface.controllers.StartRecording;
import global.userInterface.controllers.StopRecording;
import global.userInterface.view.ViewModel;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import global.model.AppModel;


public class RecordingPanelButtonPane extends JPanel {
	
	private final JButton loadButton, recordButton, stopButton;

	public RecordingPanelButtonPane(ViewModel model, AppModel appModel) {
		super();
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		add(Box.createHorizontalGlue());
		Icon stop = new ImageIcon("icons/stop.png");
		Icon record = new ImageIcon("icons/record.png");
		add(recordButton = new JButton(record));
		recordButton.addActionListener(new StartRecording(appModel));
		add(Box.createRigidArea(new Dimension(20, 0)));
		add(stopButton = new JButton(stop));
		stopButton.addActionListener(new StopRecording(appModel));
		add(Box.createRigidArea(new Dimension(50, 0)));
		add(loadButton = new JButton("Load music file..."));
		loadButton.addActionListener(new LoadReferenceSound(appModel));
		add(Box.createHorizontalGlue());
	}

}
