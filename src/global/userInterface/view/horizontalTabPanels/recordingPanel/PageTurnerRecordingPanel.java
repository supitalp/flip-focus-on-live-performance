package global.userInterface.view.horizontalTabPanels.recordingPanel;

import global.userInterface.view.PageTurnerImagePanel;
import global.userInterface.view.ViewModel;
import global.userInterface.view.horizontalTabPanels.EmptyPanel;

import java.awt.BorderLayout;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import global.model.AppModel;


public class PageTurnerRecordingPanel extends JPanel {
	
	private final RecordingPanelButtonPane buttonPane;
	private final PageTurnerImagePanel imagePanel;
	
	public PageTurnerRecordingPanel(ViewModel model, AppModel appModel) throws Exception{
		super();
		this.setLayout(new BorderLayout());
		add(buttonPane = new RecordingPanelButtonPane(model, appModel), BorderLayout.NORTH);
		add(imagePanel = new PageTurnerImagePanel(model, appModel), BorderLayout.CENTER);
		add(new EmptyPanel(0,150,10,150), BorderLayout.EAST);
		add(new EmptyPanel(0,150,10,150), BorderLayout.WEST);
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
	
	}

	public void notifyForUpdate() {
		imagePanel.notifyForUpdate();
		
	}

}
