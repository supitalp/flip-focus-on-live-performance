package global.userInterface.view.horizontalTabPanels;

import java.io.IOException;

import global.userInterface.view.ViewModel;
import global.userInterface.view.horizontalTabPanels.livePlayPanel.PageTurnerLivePlayPanel;
import global.userInterface.view.horizontalTabPanels.recordingPanel.PageTurnerRecordingPanel;
import global.userInterface.view.horizontalTabPanels.taggingPanel.PageTurnerTaggingPanel;

import javax.swing.JTabbedPane;

import global.model.AppModel;


public class PageTurnerHorizontalTab extends JTabbedPane{
	//gère les onglets horizontaux : indiquent les modules d'une instance du programme
	private final PageTurnerRecordingPanel recordingPanel;
	private final PageTurnerTaggingPanel taggingPanel;
	private final PageTurnerLivePlayPanel livePlayPanel;

	public PageTurnerHorizontalTab(ViewModel model,AppModel appModel) throws Exception{
		this.recordingPanel = new PageTurnerRecordingPanel(model, appModel);
		this.taggingPanel = new PageTurnerTaggingPanel(model, appModel);
		this.livePlayPanel = new PageTurnerLivePlayPanel(model, appModel);
		this.addTab("Acquisition!", recordingPanel );//à changer pour ajouter des icones et un texte décrivant le contenu de l'onglet
		this.addTab("Tag!", taggingPanel);
		this.addTab("Play!", livePlayPanel);
	}

	public void notifyForUpdate() {
		recordingPanel.notifyForUpdate();
		
	}
}
