package global.userInterface.view.horizontalTabPanels.taggingPanel;

import global.model.AppModel;
import global.userInterface.controllers.RunTagging;
import global.userInterface.view.ViewModel;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;


public class TaggingPanelNorthButtonPane extends JPanel{

	private final UpperButtonPane upperPane;
	private final LowerButtonPane lowerPane;
	
	public TaggingPanelNorthButtonPane(ViewModel model, AppModel appModel){
		// TODO Auto-generated constructor stub
		super();
		setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		setLayout(new GridLayout(2,0));
		add(upperPane = new UpperButtonPane(model, appModel));
		add(lowerPane = new LowerButtonPane(model,appModel));
		
	}

}
