package global.userInterface.view.horizontalTabPanels.taggingPanel;

import global.model.AppModel;
import global.userInterface.view.ViewModel;
import global.userInterface.view.PDFUtilities.PagePanel;
import global.userInterface.view.horizontalTabPanels.EmptyPanel;
import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;



public class PageTurnerTaggingPanel extends JPanel {
	
	private final TaggingPanelNorthButtonPane northButtonPane;
	private final TaggingPanelEastButtonPane eastButtonPane;
	private final TaggingPanelWestButtonPane westButtonPane;
	private final TaggingPanelSouthPanel southPanel;
	private final PagePanel PDFPanel;
	private final ViewModel model;
	

	public PageTurnerTaggingPanel(ViewModel model, AppModel appModel){
		super();
		this.model = model;
		this.setLayout(new BorderLayout());
		add(northButtonPane = new TaggingPanelNorthButtonPane(model, appModel), BorderLayout.NORTH);
		add(PDFPanel = model.getPDFPanel(), BorderLayout.CENTER);
		add(eastButtonPane = new TaggingPanelEastButtonPane(model, appModel), BorderLayout.EAST);
		add(westButtonPane = new TaggingPanelWestButtonPane(model, appModel), BorderLayout.WEST);
		add(southPanel = new TaggingPanelSouthPanel(model,appModel), BorderLayout.SOUTH);
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
	
	}

}
