package global.userInterface.view.horizontalTabPanels.taggingPanel;

import global.model.AppModel;
import global.userInterface.controllers.RunTagging;
import global.userInterface.controllers.StopTagging;
import global.userInterface.view.ViewModel;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class LowerButtonPane  extends JPanel{
	
	private final JButton stopTaggingButton, startTaggingButton;
	
	public LowerButtonPane(ViewModel model, AppModel appModel){
		// TODO Auto-generated constructor stub
		super();
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		Icon play = new ImageIcon("icons/play.png");
		Icon stop = new ImageIcon("icons/stop.png");
		c.insets = new Insets(5,0,0,0);
		c.gridwidth = 1;
		add(Box.createHorizontalGlue(),c);
		c.gridwidth = 1;
		add(Box.createRigidArea(new Dimension(20,0)),c);
		c.gridwidth = 1;
		add(startTaggingButton = new JButton(play),c);
		startTaggingButton.addActionListener(new RunTagging(appModel));
		c.gridwidth = 1;
		startTaggingButton.addActionListener(new RunTagging(appModel));
		add(Box.createRigidArea(new Dimension(15,0)),c);
		c.gridwidth = 1;
		add(stopTaggingButton = new JButton(stop),c);
		stopTaggingButton.addActionListener(new StopTagging(appModel));
		c.gridwidth = GridBagConstraints.PAGE_END;
		add(Box.createHorizontalGlue(),c);
	}

}
