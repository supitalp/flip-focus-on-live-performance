package global.userInterface.view.horizontalTabPanels.taggingPanel;

import global.model.AppModel;
import global.userInterface.controllers.TagToNextPage;
import global.userInterface.view.ViewModel;

import java.awt.Dimension;

import global.userInterface.controllers.TagToNextPage;
import global.userInterface.view.ViewModel;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import global.model.AppModel;

public class TaggingPanelWestButtonPane extends JPanel{
	
	private final JButton previousPageButton;
	

	public TaggingPanelWestButtonPane(ViewModel model, AppModel appModel){
		// TODO Auto-generated constructor stub
		super();
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		setBorder(BorderFactory.createEmptyBorder(0, 50, 10, 50));
		add(Box.createRigidArea(new Dimension(50,10)));
		add(Box.createHorizontalGlue());
		add(Box.createRigidArea(new Dimension(50,10)));
		//add(Box.createVerticalGlue());
		add(previousPageButton = new JButton("<"));
		previousPageButton.addActionListener( model.getPrevAction());
		previousPageButton.addActionListener(new TagToNextPage(appModel));
		//add(Box.createVerticalGlue());
		add(Box.createRigidArea(new Dimension(50,10)));
		add(Box.createHorizontalGlue());
		add(Box.createRigidArea(new Dimension(50,10)));
	
	}

}
