package global.userInterface.view.horizontalTabPanels.taggingPanel;

import global.model.AppModel;
import global.userInterface.controllers.LoadTagFile;
import global.userInterface.view.ViewModel;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
/**This class is used to organize the elements on the second row in the NorthButtonPane.
 * It uses the flexible GridBagLayout to display the elements
 * 
 * @author Adelin Travers
 *
 */
public class UpperButtonPane extends JPanel{
	
	private final JButton loadPDFButton,loadTagButton;
	/**Constructor of the UpperButtonPane
	 * 
	 * @param model
	 * @param appModel
	 */
	public UpperButtonPane(ViewModel model, AppModel appModel){
		// TODO Auto-generated constructor stub
		super();
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		add(Box.createHorizontalGlue(),c);
		add(loadPDFButton = new JButton("Load music sheet..."),c);
		loadPDFButton.addActionListener(model.getOpenAction());
		add(Box.createRigidArea(new Dimension(50, 0)),c);
		add(loadTagButton = new JButton("Load Tag file..."),c);
		loadTagButton.addActionListener(new LoadTagFile(appModel));
		c.gridwidth = GridBagConstraints.PAGE_END;
		add(Box.createHorizontalGlue(),c);
	}


}
