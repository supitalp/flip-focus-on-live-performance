package global.userInterface.view.horizontalTabPanels.taggingPanel;

import global.userInterface.controllers.TagToNextPage;
import global.userInterface.view.ViewModel;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import global.model.AppModel;

public class TaggingPanelEastButtonPane extends JPanel{
	
	private final JButton nextPageButton;
	

	public TaggingPanelEastButtonPane(ViewModel model, AppModel appModel){
		// TODO Auto-generated constructor stub
		super();
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		//add(Box.createVerticalGlue());
		add(Box.createRigidArea(new Dimension(50,10)));
		add(Box.createHorizontalGlue());
		add(Box.createRigidArea(new Dimension(50,10)));
		add(nextPageButton = new JButton(">"));
		nextPageButton.addActionListener( model.getNextAction());
		nextPageButton.addActionListener(new TagToNextPage(appModel));
		add(Box.createRigidArea(new Dimension(50,10)));
		add(Box.createHorizontalGlue());
		add(Box.createRigidArea(new Dimension(50,10)));
		//add(Box.createVerticalGlue());
		setBorder(BorderFactory.createEmptyBorder(0, 50, 10, 50));
	
	}

}
