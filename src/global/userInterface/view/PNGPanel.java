package global.userInterface.view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import global.model.AppModel;

import javax.swing.JPanel;
import javax.swing.JTextPane;

public class PNGPanel extends JPanel{
	
	private AppModel appModel;
	
	private PNGModel pngModel;
	
	private BufferedImage score;
	
	private int pageNumber;
	
	public PNGPanel(AppModel appModel, PNGModel pngModel) {
		super();
		this.appModel = appModel;
		this.pngModel = pngModel;
		score = null;
		pageNumber = -1;
	}
	
	public void setPageNumber(int i) {
		this.pageNumber = i;
		this.score = this.pngModel.getPNGList().get(i);
		repaint();
	}
	
	public int getPageNumber() {
		return this.pageNumber;
	}
	@Override
    public void paintComponent(Graphics g) {
			g.drawImage(score, 0, 0, null); // see javadoc for more info on the parameters
    }
}
