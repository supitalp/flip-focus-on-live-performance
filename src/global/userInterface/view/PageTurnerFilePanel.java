package global.userInterface.view;

import global.model.AppModel;
import global.userInterface.view.horizontalTabPanels.PageTurnerHorizontalTab;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
@Deprecated
/**
 * This class will be used in further developments to support multiple files at the same time.
 * One FilePanel will be used for each file opened at once.
 * It is set as Deprecated for now since it seems we won't have time to develop this in the final version.
 * @author Adelin Travers
 *
 */
public class PageTurnerFilePanel extends JPanel{
	
	private final PageTurnerHorizontalTab horizontalTabbedPane;
	/**Constructor of PageTurnerFilePanel
	 * 
	 * @param viewModel
	 * @param appModel
	 * @throws Exception 
	 */
	public PageTurnerFilePanel(ViewModel viewModel, AppModel appModel) throws Exception{
		super();
		horizontalTabbedPane = new PageTurnerHorizontalTab(viewModel, appModel);
		setLayout(new BorderLayout());
		this.add(horizontalTabbedPane , BorderLayout.CENTER);
		setPreferredSize(new Dimension(256,256));
		
	}

}
