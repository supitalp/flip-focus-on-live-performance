package global.userInterface.view;

import java.io.IOException;

import global.model.AppModel;
import global.userInterface.view.horizontalTabPanels.PageTurnerHorizontalTab;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
/**
 * Panel that contains the main features of the frame.
 * Used to organize the UI outlook.
 * 
 * @author Adelin Travers
 *
 */

public class PageTurnerWindowPanel extends JPanel{
	
	private final PageTurnerHorizontalTab horizontalTabbedPane;
	/**Constructor of the WindowPanel. Sets up the tab display.
	 * 
	 * @param viewModel
	 * @param appModel
	 * @throws Exception 
	 */
	public PageTurnerWindowPanel(ViewModel viewModel, AppModel appModel) throws Exception{
		super();
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		horizontalTabbedPane = new PageTurnerHorizontalTab(viewModel, appModel);
		add(horizontalTabbedPane);

		//PageTurnerVerticalTab newVerticalTabbedPane = new PageTurnerVerticalTab("new File", viewModel, appModel);
		//this.add(newVerticalTabbedPane);
	}
	public void notifyForUpdate() {
		horizontalTabbedPane.notifyForUpdate();
		
	}
}
