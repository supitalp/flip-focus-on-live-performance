package global.userInterface.view;

import global.model.AppModel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.JPanel;

/**This class is used to display the plots of the chroma vectors.
 * 
 * @author Adelin Travers and Issam Outassourt
 *
 */

public class PageTurnerImagePanel extends JPanel {
	
	private final PlotPanel plotPane;
	private final SubImagePanel subImage;
	/**Constructor of the ImagePanel.
	 * Divided in two sub-panels.
	 * 
	 * @param model
	 * @param appModel
	 * @throws Exception 
	 */
	public PageTurnerImagePanel(ViewModel model, AppModel appModel) throws Exception {
		super();
		setPreferredSize(new Dimension(500,500));
		add(plotPane = new PlotPanel(model,appModel), BorderLayout.NORTH);
		add(subImage = new SubImagePanel(model,appModel), BorderLayout.SOUTH);
	}
	public void notifyForUpdate() {
		plotPane.notifyForUpdate();
		
	}

}
