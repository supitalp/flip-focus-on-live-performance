package global.userInterface.view;

import java.awt.image.BufferedImage;

import global.model.AppModel;

import javax.swing.JPanel;
import javax.swing.JTextPane;

public class JPGPanel extends JPanel{
	
	private AppModel appModel;
	
	private JPGModel jpgModel;
	
	private BufferedImage score;
	
	private int pageNumber;
	
	private JTextPane constPage, pageField, totalPage;
	
	public JPGPanel(AppModel appModel, JPGModel jpgModel) {
		super();
		this.appModel = appModel;
		this.jpgModel = jpgModel;
		score = null;
		pageNumber = -1;
		constPage = new JTextPane();
		constPage.setText("Page ");
		constPage.setEditable(false);
		pageField = new JTextPane();
		pageField.setText("0");
		pageField.setEditable(false);
		totalPage = new JTextPane();
		totalPage.setText("of 0");
		totalPage.setEditable(false);
	}
	
	
}
