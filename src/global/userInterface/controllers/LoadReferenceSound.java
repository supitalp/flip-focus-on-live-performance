package global.userInterface.controllers;

import global.audio.PCMFilePlayer;
import global.userInterface.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFileChooser;

import global.matching.Matching;
import global.matching.Path;
import global.model.*;

public class LoadReferenceSound implements ActionListener {
	private final AppModel appModel;
	
	public LoadReferenceSound(AppModel appModel) {
		super();
		this.appModel = appModel;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		JFileChooser chooser = new JFileChooser ();
		int returnPath = chooser.showOpenDialog(null);
		if(returnPath == JFileChooser.APPROVE_OPTION) {
			File fileChoosed = chooser.getSelectedFile();
			appModel.setFilenameRef(fileChoosed.getPath());
			appModel.setRef(appModel.getIoManagement().loadSignalFromPCM(fileChoosed.getPath()));
			appModel.setVecRef(appModel.getDsp().calculateChromaFrom(appModel.getRef()));
			try {
				appModel.getIoManagement().writeChromaFile(appModel.getVecRef(), fileChoosed.getName() + ".ref.chr");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (appModel.getPlay() != null && appModel.getVecPlay() != null) {
				Matching matching = new Matching(appModel.getVecPlay(), appModel.getVecRef());
				appModel.setMatching(matching);
				Path matchingPath = matching.match();
				appModel.setMatchingPath(matchingPath);
			}
		}
		
		/*
		 * idem que pour loadlivesound
		 * on fait le matching quand les deux fichiers sont definis
		 */
	}
}
