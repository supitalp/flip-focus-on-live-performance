package global.userInterface.controllers;

import global.audio.PCMFilePlayer;
import global.userInterface.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFileChooser;

import global.matching.Matching;
import global.matching.Path;
import global.model.*;

public class LoadLiveSound implements ActionListener {
private final AppModel appModel;
	
	public LoadLiveSound(AppModel appModel) {
		super();
		this.appModel = appModel;
	}
	
	/**
	 * 
	 */
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		JFileChooser chooser = new JFileChooser ();
		int returnPath = chooser.showOpenDialog(null);
		if(returnPath == JFileChooser.APPROVE_OPTION) {
				File fileChoosed = chooser.getSelectedFile();
				appModel.setFilenamePlay(fileChoosed.getPath());
				appModel.setPlay(appModel.getIoManagement().loadSignalFromPCM(fileChoosed.getPath()));
				appModel.setVecPlay(appModel.getDsp().calculateChromaFrom(appModel.getPlay()));
				try {
					appModel.getIoManagement().writeChromaFile(appModel.getVecPlay(), fileChoosed.getName() + ".live.chr");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (appModel.getRef() != null && appModel.getVecRef() != null) {
					Matching matching = new Matching(appModel.getVecPlay(), appModel.getVecRef());
					appModel.setMatching(matching);
					Path matchingPath = matching.match();
					//Path matchingPath = matching.naiveMatch();
					matching.outputToImage("dtw.bmp", true);
					//matching.outputToImage("naive.bmp", true);
					appModel.setMatchingPath(matchingPath);
				}
		}
		
		/*mettre à jour les références avec play dans l'appmodel
		 * load signal play avec fonction dans iomanagement
		 * calculer et mettre à jour les tableaux de vecteurs chroma, dsp vecplay signaltochroma
		 * faire un write tag file sur l'objet iomanagement
		 * 
		 * Matching matching = new Matching(vecPlay, vecRef);
		 * matchingPath = matching.match();
		 * 
		 * matching.output();
		 * Penser à récuperer la biblio de dessin
		 */
	}
}
