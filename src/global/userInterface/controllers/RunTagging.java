package global.userInterface.controllers;

import global.audio.PCMFilePlayer;
import global.userInterface.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFileChooser;

import global.model.*;

public class RunTagging implements ActionListener {
private final AppModel appModel;
	
	public RunTagging(AppModel appModel) {
		super();
		this.appModel = appModel;
	}
	
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		appModel.getTagClock().reset();
		appModel.getTags().clear();
		try {
			PCMFilePlayer pcmFilePlayer = new PCMFilePlayer(new File(appModel.getFilenameRef()));
			appModel.setAudioOut(pcmFilePlayer);
			appModel.getAudioOut().start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * recuperer l'objet de type clock sur git
	 * new PCMFilePlayer(new File(filenameRef));
	 * tagClock.reset()
	 * 
	 */
}
