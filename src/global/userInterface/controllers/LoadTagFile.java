package global.userInterface.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFileChooser;

import global.IOManagement.IOManagement;
import global.matching.Matching;
import global.matching.Path;
import global.model.AppModel;

public class LoadTagFile implements ActionListener {
	
	private final AppModel appModel;
	
	public LoadTagFile(AppModel appModel) {
		super();
		this.appModel = appModel;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
		JFileChooser chooser = new JFileChooser ();
		int returnPath = chooser.showOpenDialog(null);
		if(returnPath == JFileChooser.APPROVE_OPTION) {
			File fileChoosed = chooser.getSelectedFile();
			IOManagement ioM = new IOManagement();
			try {
				appModel.setTags(ioM.loadTagsFromTagFile(fileChoosed.getAbsolutePath()));
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
