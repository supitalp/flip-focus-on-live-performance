package global.userInterface.controllers;

import global.audio.PCMFilePlayer;
import global.userInterface.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;

import global.model.*;

public class TagToNextPage implements ActionListener {
private final AppModel appModel;
	
	public TagToNextPage(AppModel appModel) {
		super();
		this.appModel = appModel;
	}
	
	public void actionPerformed(ActionEvent arg0) {
		if (appModel.getAudioOut().isNotYetEOF()) {
			appModel.getTags().add(new Tag((long)appModel.getTagClock().getElapsedTime(), "", appModel.getViewModel().getCurpage()+1));
		}
	}
	
	/*
	 * tagButtonTriggered
	 * recup la position avec tagClock.getElapsedTime()
	 */
}
