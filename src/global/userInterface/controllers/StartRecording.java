package global.userInterface.controllers;

import global.model.AppModel;
import global.model.VectorChroma;
import global.queues.Queues;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartRecording implements ActionListener{
	AppModel appModel;
	
	public StartRecording(AppModel appModel) {
		super();
		this.appModel = appModel;
	}

	public void actionPerformed(ActionEvent e) {
		appModel.getLiveMicro().startRecording(appModel.timeToWait);
		appModel.getLiveDSP().startComputing();
		Runnable receiveVC = new Runnable() {
			VectorChroma tmp;
			public void run () {
				while ((tmp = Queues.DSPToPlotByVectorChromaQueue.pop()) != null) {
					synchronized(appModel.LOCK) {
						appModel.vc = tmp;
					}
					appModel.repaint();
				}
			}
		};
		Thread plotter = new Thread(receiveVC);
		plotter.start();
		
	}
	
}
