package global.userInterface.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFileChooser;

import global.model.AppModel;

public class StopTagging implements ActionListener {
	AppModel appModel;
	
	public StopTagging(AppModel appModel) {
		super();
		this.appModel = appModel;
	}

	public void actionPerformed(ActionEvent e) {
		if (!appModel.getTags().isEmpty()) {
			JFileChooser chooser = new JFileChooser();
			int returnPath = chooser.showSaveDialog(null);
			if (returnPath == JFileChooser.APPROVE_OPTION) {
				File fc = chooser.getSelectedFile();
				appModel.getAudioOut().stop();
				try {
					appModel.getIoManagement().writeTagFile(appModel.getTags(), fc.getPath());
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}

}
