package global.userInterface.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.swing.JFileChooser;

import global.matching.Matching;
import global.matching.Path;
import global.model.AppModel;

public class StopRecording implements ActionListener {
	AppModel appModel;
	
	public StopRecording(AppModel appModel) {
		super();
		this.appModel = appModel;
	}

	public void actionPerformed(ActionEvent e) {
		if(appModel.getLiveMicro().getRunning()) {
		AudioInputStream ais = null;
		try {
			ais = appModel.getLiveMicro().StopRecording();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		JFileChooser chooser = new JFileChooser ();
		int returnPath = chooser.showSaveDialog(null);
		if(returnPath == JFileChooser.APPROVE_OPTION) {
			File fc = chooser.getSelectedFile();
			try {
				AudioSystem.write(ais, AudioFileFormat.Type.WAVE, new File(fc.getPath()));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			appModel.setFilenameRef(fc.getPath());
			appModel.setRef(appModel.getIoManagement().loadSignalFromPCM(fc.getPath()));
			appModel.setVecRef(appModel.getDsp().calculateChromaFrom(appModel.getRef()));
			try {
				appModel.getIoManagement().writeChromaFile(appModel.getVecRef(), fc.getName() + ".ref.chr");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (appModel.getPlay() != null && appModel.getVecPlay() != null) {
				Matching matching = new Matching(appModel.getVecPlay(), appModel.getVecRef());
				appModel.setMatching(matching);
				Path matchingPath = matching.match();
				appModel.setMatchingPath(matchingPath);
			}
		}
		}
	}

}
