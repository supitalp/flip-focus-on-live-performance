package global.userInterface.controllers;

import global.audio.PCMFilePlayer;
import global.externalPrograms.ExternalPrograms;
import global.userInterface.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFileChooser;

import global.model.*;

public class LoadPDFFile implements ActionListener {
private final AppModel appModel;
	
	public LoadPDFFile(AppModel appModel) {
		super();
		this.appModel = appModel;
	}
	
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		JFileChooser chooser = new JFileChooser ();
		int returnPath = chooser.showOpenDialog(null);
		if(returnPath == JFileChooser.APPROVE_OPTION) {
			File fc = chooser.getSelectedFile();
			ExternalPrograms ext;
			try {
				ext = new ExternalPrograms ();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				ext = null;
				e.printStackTrace();
			}
			try {
				String fileName = ext.convertPDFToImages(fc.getPath());
				int lengthName = fileName.length();
				String prefixName = fileName.substring(0, lengthName-4);
				ArrayList<File> fileList = new ArrayList<File>();
				int i = 0;
				File f = null;
				while((f = new File(prefixName+"-"+i+".png")).exists()) {
					i=i+1;
					fileList.add(f);
				}
				appModel.getViewModel().getPNGModel().setPNGFileList(fileList);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/*
		 * Charger les pages pdf
		 * Penser a definir la fonction turnpage
		 */
	}
}
