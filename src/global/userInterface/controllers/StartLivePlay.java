package global.userInterface.controllers;

import global.audio.PCMFilePlayer;
import global.userInterface.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;

import global.matching.LiveMatching;
import global.matching.LiveReplay;
import global.model.*;

public class StartLivePlay implements ActionListener {
private final AppModel appModel;
	
	public StartLivePlay(AppModel appModel) {
		super();
		this.appModel = appModel;
	}
	
	public void actionPerformed(ActionEvent arg0) {
		Tag[] tags = new Tag[appModel.getTags().size()];
		appModel.getTags().toArray(tags);
		LiveMatching liveMatching = new LiveMatching(appModel.getVecRef(),tags,appModel);
		appModel.setLiveMatching(liveMatching);
		Thread liveMatch = new Thread(liveMatching);
		liveMatch.start();
		appModel.getLiveMicro().startRecording(0);
		appModel.getLiveDSP().startComputingToMatch();
		appModel.turnPage(0);
		appModel.getTagClock().reset();
	}
	
	/*
	 * lancer une instance de liveReplay
	 * appelle lui meme la methode turn page qu'il va falloir définir
	 * voir branche henri
	 */
}
