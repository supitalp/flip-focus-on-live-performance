package global.userInterface.controllers;

import global.model.AppModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

public class StopLiveSound implements ActionListener {
	
	private AppModel appModel;
	
	public StopLiveSound(AppModel appModel) {
		super();
		this.appModel = appModel;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		JFileChooser chooser = new JFileChooser();
		int returnPath = chooser.showSaveDialog(null);
		if (returnPath == JFileChooser.APPROVE_OPTION) {
			File fc = chooser.getSelectedFile();		
			appModel.getLiveMatching().stop(fc.getAbsolutePath());
		}
	}
}
