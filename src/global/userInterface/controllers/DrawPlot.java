package global.userInterface.controllers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import global.model.VectorChroma;

import javax.swing.JPanel;

public class DrawPlot {
	private static String[] chromas = {"A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#",
	                            "G", "G#"};
	private static String titleAF = "Plot VectorChroma";
	public static void graphicAF(JPanel plotPanel, VectorChroma vectorAF, Graphics graphics) {
		if (vectorAF == null)
			return;
		
		/*The y Axis value represents y / ymax, though we should get the min and
		 * max value
		 */
		double minValue = 0;
		double maxValue = 0;
		for (int i = 0; i < 12; i++) {
			if (minValue > vectorAF.get(i))
				minValue = vectorAF.get(i);
			if (maxValue < vectorAF.get(i))
				maxValue = vectorAF.get(i);
		  }
		Dimension dim = plotPanel.getSize();
		int clientWidth = dim.width;
		int clientHeight = dim.height;
		int barWidth = clientWidth / 12;
		Font titleFont = new Font("Book Antiqua", Font.BOLD, 15);
		FontMetrics titleFontMetrics = graphics.getFontMetrics(titleFont);
		Font labelFont = new Font("Book Antiqua", Font.PLAIN, 10);
		FontMetrics labelFontMetrics = graphics.getFontMetrics(labelFont);
		
		int titleWidth = titleFontMetrics.stringWidth(titleAF);
		int q = titleFontMetrics.getAscent();
		int p = (clientWidth - titleWidth) / 2;
		graphics.setFont(titleFont);
		graphics.drawString(titleAF, p, q);
		
		int top = titleFontMetrics.getHeight();
		int bottom = labelFontMetrics.getHeight();
		if (maxValue == minValue)
		return;
		
		double scale = (clientHeight - top - bottom) / (maxValue - minValue);
		q = clientHeight - labelFontMetrics.getDescent();
		graphics.setFont(labelFont);
		
		
		for (int i = 0; i < 12; i++) {
			int j = (i+9)%12;
			int valueP = i * barWidth + 1;
			int valueQ = top;
			int height = (int) (vectorAF.get(j) * scale);
			if (vectorAF.get(j) >= 0)
				valueQ += (int) ((maxValue - vectorAF.get(j)) * scale);
			else {
				valueQ += (int) (maxValue * scale);
				height = -height;
			}
		
		graphics.setColor(Color.blue);
		graphics.fillRect(valueP, valueQ, barWidth - 2, height);
		graphics.setColor(Color.black);
		graphics.drawRect(valueP, valueQ, barWidth - 2, height);
		int labelWidth = labelFontMetrics.stringWidth(chromas[j]);
		p = i * barWidth + (barWidth - labelWidth) / 2;
		graphics.drawString(chromas[j], p, q);
		
		}
	}
}
